class BigOrSmall extends UIComponent  {
	private arrayCollection: eui.ArrayCollection;
	public constructor() {
		super();
		
	}

	protected partAdded(partName: string, instance: any): void {
		super.partAdded(partName, instance);
	}


	protected childrenCreated(): void {
		super.childrenCreated();
		this.createUI();
	}
	public createUI(): void {
		let data: BigOrSmallItemData[] = [];
		for (let i = 0; i < 4; i++) {
			data.push(new BigOrSmallItemData(i as BigOrSmallType));
		}
		this.arrayCollection = new eui.ArrayCollection(data);
		this.uiList.itemRenderer = BigOrSmallItem;
		this.uiList.dataProvider = this.arrayCollection;
		this.arrayCollection.refresh();
	}
}