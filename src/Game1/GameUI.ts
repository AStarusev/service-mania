class GameUI extends eui.Component implements eui.UIComponent {
	private addBet: eui.Button;
	private submit: eui.Button;
	private betNum: BetInput;
	private backBtn: eui.Label;
	private betInfo: eui.Label;
	private gameNameLabel: eui.Label;
	private prompt: eui.Label;
	private balance: eui.Label;
	private time: eui.Label;
	private phase: eui.Label;
	private history:eui.Label;
	private component: UIComponent
	private betData: SettlementItemData;
	public constructor() {
		super();
		setInterval(()=>{
			this.setBalance()
		},5000);
	}

	protected partAdded(partName: string, instance: any): void {
		super.partAdded(partName, instance);
	}


	protected childrenCreated(): void {
		super.childrenCreated();
		this.createUI();
	}
	private createUI(): void {
		this.phase.text = "搅珠期数 " + Game1.phase;
		this.time.text = "截止售票时间 " + Game1.time;
		this.addBet.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onTouchAddBet, this);
		this.submit.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onTouchSubmit, this);
		this.backBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onTouchBack, this);
		this.history.addEventListener(egret.TouchEvent.TOUCH_TAP, ()=>{
			UIManager.showHistory();
		}, this);
		
		this.addEventListener(GameUIEvent.UPDATE, this.onUpdateUI, this);
		this.addEventListener(GameUIEvent.UPDATE_USERINFO, () => { 
			this.setBalance();
			this.setGameTimeAndPhase();
		}, this);

		this.setBalance();
		this.setGameTimeAndPhase();
	}

	private setBalance(){
		// Get the balance and handle case where lottery is loading faster than balance being set in proxy
		let bal: number = NetManager.getbalance();
		if(bal < 0){
			console.log('balance not valid, setting timeout to refetch');
			// Force another call to get the balance
			setTimeout(() => {this.dispatchEvent(new GameUIEvent(GameUIEvent.UPDATE_USERINFO));}, 1000)
		} 
		
		if(this.balance){
			this.balance.text = Util.numToShow(bal, 2);
		}
	}

	private setGameTimeAndPhase(){
		Game1.updateData();
		if(!Game1.time || Game1.time.length  === 0){
			console.log('Game time and phase not valid, setting timeout to refetch');
			setTimeout(() => {this.dispatchEvent(new GameUIEvent(GameUIEvent.UPDATE_USERINFO));}, 1000)
		}

		this.phase.text = "搅珠期数 " + Game1.phase;
		this.time.text = "截止售票时间 " + Game1.time;
	}

	private onUpdateUI(e: GameUIEvent): void {
		// ====
		// TODO: check unput value, compare it with maxBet and minBet in lotteryConfig in GameData.ts
		// and update UI value 
		console.log('____', this.betNum.getInputNum(), this.betNum);
		if (e.itemType == -1) {
			this.addBet.enabled = false;
			this.betData = null;
			this.betInfo.text = "请正确押注";
		} else if (e.itemType == undefined) {
			if (this.betData) {
				this.betData.setBetNum(this.betNum.getInputNum());
				this.addBet.enabled = this.betNum.getInputNum() != 0;
				this.betInfo.text = "押注 " + this.betData.name + "  奖金 " + Util.numToShow(this.betData.winNum, 2);
			}
		}
		else {
			if (!this.betData) {
				this.betData = new SettlementItemData(this.component.gameType);
			}
			this.betData.itemType = e.itemType;
			this.betData.data = e.data;
			this.betData.name = e.name;
			this.betData.setBetNum(this.betNum.getInputNum());
			this.addBet.enabled = this.betNum.getInputNum() != 0;
			this.betInfo.text = "押注 " + e.name + "  奖金 " + Util.numToShow(this.betData.winNum, 2);
		}
	}
	private onTouchAddBet(e?: egret.TouchEvent): void {
		if (this.addBet.enabled) {
			this.component.dispatchEvent(new UIComponentEvent(UIComponentEvent.RESET));
			Game1.getSettlementManager().addData(this.betData);
			this.addBet.enabled = false;
			this.betData = null;
			this.betInfo.text = "押注  奖金 " + Util.numToShow(0);
		}
	}
	private onTouchSubmit(e: egret.TouchEvent): void {
		this.onTouchAddBet();
		UIManager.showSettlementUI();
	}
	public setUIComponent(component: UIComponent): void {
		this.component = component;
		this.gameNameLabel.text = this.component.gameNameStr;
		this.prompt.text = Game1Config.GameInfo.gameNameConfig[this.component.gameType].prompt;
	}
	private onTouchBack(e: egret.TouchEvent): void {
		this.parent.parent.dispatchEvent(new UIComponentEvent(UIComponentEvent.UPDATEUI));
		//this.parent.parent.removeChild(this.parent);
		this.parent.dispatchEvent(new UIComponentEvent(UIComponentEvent.REMOV_UICOMPONENT));
	}

}
class GameUIEvent extends egret.Event {
	public static UPDATE: string = "Update";
	public static UPDATE_USERINFO: string = "Update_UserInfo";
	public itemType: number;
	public data: number[];
	public name: string;
	public constructor(type: string, bubbles: boolean = false, cancelable: boolean = false) {
		super(type, bubbles, cancelable);
	}
}