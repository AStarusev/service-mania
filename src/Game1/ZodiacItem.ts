class ZodiacItem extends UIItemRenderer {
	private touchGroup:eui.Group;
	public constructor() {
		super();
	}
	protected partAdded(partName:string,instance:any):void
	{
		super.partAdded(partName,instance);
	}
	protected childrenCreated():void
	{
		super.childrenCreated();
		this.setSymbolGroupLayout(new MiddleTableLayout(4,-8,-6,50,50));
	}
}