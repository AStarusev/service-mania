class WaveSize extends UIComponent  {
	private arrayCollection: eui.ArrayCollection;
	public constructor() {
		super();
	}

	protected partAdded(partName:string,instance:any):void
	{
		super.partAdded(partName,instance);
	}


	protected childrenCreated():void
	{
		super.childrenCreated();
		let data: WaveSizeItemData[] = [];
		for (let i = 0; i < 6; i++) {
			data.push(new WaveSizeItemData(i as WaveSizeType));
		}
		this.arrayCollection = new eui.ArrayCollection(data);
		this.uiList.itemRenderer = WaveSizeItem;
		this.uiList.dataProvider = this.arrayCollection;
		this.arrayCollection.refresh();
		
	}
	
}