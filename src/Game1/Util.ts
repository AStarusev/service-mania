module Util {
	export class Util {
		public constructor() {
		}
	}
	export function numToString(num: number): string {
		let res: string = num + "";
		if (res.length >= 2) {

		} else {
			res = '0' + res;
		}
		return res;
	}
	
	export function joinString(joinStr: string, ...str: string[]): string {
		for (let item in str) {
			str[item] = str[item].replace(str[item][0], str[item][0].toLowerCase());
		}
		return str.join(joinStr);
	}

	export function numToShow(data:number|string,fixed:number=0) {
		data=Number(data)||0;
		if (data != null) {
			return Game1.moneyType+data.toFixed(fixed).replace(/\d{1,3}(?=(\d{3})+(\.\d*)?$)/g, '$&,');
		} else {
			return Game1.moneyType + "0";
		}
	}
}
