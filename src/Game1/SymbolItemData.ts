class SymbolItemData {
	public specialCode: string;
	public colorId: number;
	public textColor: number;
	public upSideColor: number;
	public upBgColor: number;
	public selectSideColor: number;
	public selectBgColor: number;
	public downHintSource: string;
	public constructor(data: SpecialCodeIFS = null) {
		if (data!=null) {
			this.setData(data);
		}

	}
	public setColorId(id: SymbolColor): void {
		this.colorId = id;
		let textColor: number[] = [0xdf0101, 0x0047d6, 0x197300,0xffffff];
		let upSideColor: number[] = [0xff0000, 0x0065e0, 0x09a11f,0xe88915];
		let upBgColor: number[] = [0xf4e7e7, 0xe9f0f5, 0xffffff,0x000000];
		let selectSideColor: number[] = [0xff3232, 0x4a79fa, 0x1c9c29,0x000000];
		let selectBgColor: number[] = [0x760000, 0x09256e, 0x003d00,0x000000];
		let downHintSource: string[] = ["symbol_red_png", "symbol_bule_png", "symbol_green_png"];
		this.textColor = textColor[id];
		this.upSideColor = upSideColor[id];
		this.upBgColor = upBgColor[id];
		this.selectSideColor = selectSideColor[id];
		this.selectBgColor = selectBgColor[id];
		this.downHintSource = downHintSource[id];
	}
	public setData(data: SpecialCodeIFS): void {
		this.specialCode = Util.numToString(data.specialCode);
		this.setColorId(data.colorId);
	}
}