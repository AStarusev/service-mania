module NetManager {
    /* 
         enum GameType {
            Symbol, OddEven, Zodiac, BigOrSmall, RangeOfColor, ColorSize, NormalNumber
         }  
         enum Symbol {
             1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, ..., 49
         }
         enum BigOrSmallType {
            OddBig, OddSmall, EvenBig, EvenSmall
         }
         enum RangeOfColorType{
            Red,Blue,Green
         }   
         enum OddEvenType{
            Odd,Even,Big,Small,CombinedOdd,CombinedEven
         } 
         enum ColorSizeType{
            RedSmall,RedBig,BlueSmall,BlueBig,GreenSmall,GreenBig
         }
         enum ZodiacCategoryType {
            BeastZodiac, LivestockZodiac, RatSide, HorseSide, OneZodiacSign, ThreeZodiacSigns, FourZodiacSigns, FiveZodiacSigns, SixZodiacSigns
         }
         enum ZodiacType {
            Rat, Ox, Tiger, Rabbit, Dragon, Snake, Horse, Goat, Monkey, Rooster, Dog, Pig
        }
    */

    /*
        called by the lottery html to get the user’s current balance and show in the game UI
    */
    export function getbalance(): number {
        let balance: number;
        try{
            // Call into the proxy to get the current wallet balance
            var xmlHttp = new XMLHttpRequest();

            //TODO switch to async and use callbacks
            xmlHttp.open("GET", "http://localhost:8081/bbApi/balance", false);
            xmlHttp.send(null);
            var jsonResponse = JSON.parse(xmlHttp.responseText);

            console.log('balance response', jsonResponse);
            balance = jsonResponse.balance;
        }
        catch(e){
            console.log('Exception retreiving balance', e);
            balance = 0;
        }

        return balance;
    }
    /*
        balance type
    */
    export function getbalancetype(): string {
        return "¥"; //return value is a data of string;
    }
    export function getyear(): number {
        return 2018; //return value is a data of number;
    }

    export function getlotteryendtime(): string {
        // Return data in this format for UI to parse: 2018/04/25/09/15
        let lotteryEndTime: string = '';

        let activeRound: InputHistoryTrans = getActiveRound();
        if(activeRound){
            let recentRoundDate = getDateValueInSeconds(activeRound.data.r[0]);
            let activeDate = new Date(recentRoundDate * 1000);
            lotteryEndTime = activeDate.getFullYear() + '/' + ('0' + (activeDate.getMonth() + 1)).slice(-2) + '/' + ('0' + activeDate.getDate()).slice(-2) + '/' + ('0' + activeDate.getHours()).slice(-2) + '/' + ('0' + activeDate.getMinutes()).slice(-2);
        }
        return lotteryEndTime;
    }
    
    /**
     * Get the active round and then the third arg is the game ID
     */
    export function getphase(): string {
        let activeRound: InputHistoryTrans = getActiveRound();
        if(activeRound) {
            if(activeRound.data.r.length > 2){
                // round data is endtime, table, gameID
                return activeRound.data.r[2]; 
            }
        }

        return "";
    }

    /*
    */
    export function getreturnrate(): number[][] {
        let returnRate = [
            [40],       //Special number
            [0.85, 0.85, 0.85, 0.85, 0.85, 0.85], //Odd number,Even number,Big number, Small number, Combined odd, Combined even
            [0.85, 0.85, 0.85, 0.85, 10, 2.4, 1.6, 1, 0.85], //Beat zodiac signs, Livestock zodiac signs,Rat's side, Horse's side, One zodiac signs, Three zodiac signs, Four zodiac signs, Five zodiac signs, Six zodiac signs
            [2.5, 2.5, 2.5, 2.5], //Odd big number, Odd small number, Even big number, Even small number
            [1.6, 1.6, 1.6],//Range of color: Red, Blue, Green
            [3.5, 4, 4, 4, 4, 4],//Big or small number in one color: Red&small, Red&big, Blue&small, Blue&big, Green&small, Green&big
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0.7, 1] //Normal number:Rat, Ox, Tiger, Rabbit, Dragon, Snake, Horse, Goat, Monkey, Rooster, Gog, Pig
        ]
        return returnRate;
    }

    export interface BetData {
        type: Game1Type;
        itemType: number;
        data: number[];
        betNum: number;
    }

    // Used to send request back to cabbage
    class BetRequest {
        quantity: number;
        key: string;
        data: number[];
        table: number;
    }

    export function placebet(data: BetData[]): boolean {
        console.log('placebet1', data);
        console.log('placebet2', JSON.stringify(data));

        // Get the active round so that we have the table id
        let tableID = 0;
        let activeRound: InputHistoryTrans = getActiveRound();
        if(activeRound && activeRound.data.r.length > 1) {
            // round data is endtime, table, gameID
            tableID = activeRound.data.r[1]; 
        }

        // Create an array of bet requests to send into escher-wallet
        let betRequests: BetRequest[] = [];
        for (let betItem of data) {
            let req: BetRequest = new BetRequest();
            req.quantity = betItem.betNum;
            req.key = 'b';
            req.table = tableID;

            // Create an array for the data within each bet
            // Format: gameType, itemType, data array (only Zodiac has data array)
            let bet: number[] = new Array();
            bet.push(betItem.type);
            bet.push(betItem.itemType);
            if(betItem.data && betItem.data.length > 0){
                //Zodiac game uses data array for multi-select
                bet.push.apply(bet, betItem.data);
            }

            req.data = bet;
            betRequests.push(req);
		}

        console.log(betRequests);
        console.log(JSON.stringify(betRequests));

        // Call into the proxy to get the current wallet balance
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.open("POST", "http://localhost:8090/bbLottery/placebet", true);//last param is async flag
        xmlHttp.setRequestHeader("Content-Type", "application/json");
        xmlHttp.send(JSON.stringify(betRequests));

        return true; //return value is a data of boolean;
    }

    export function exitGame(): boolean {
        // Call into the proxy to get the current wallet balance
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.open("GET", "http://localhost:8090/bbLottery/exitgame", true);//last param is async flag
        xmlHttp.send(null);

        return true; //return value is a data of boolean;
    }

    export interface HistoryData {
        phase: string; // "18/025"
        time: string;  //"2018/04/25/09/15"
        lotteryNum: number[];
        dataDetail: HistoryDataDetail[];
        drawDate: number;
    }
    export interface HistoryDataDetail {
        time: string; //"2018/04/25/09/15"
        betData: HistoryBetData[];
    }
    export interface HistoryBetData {
        type: number;
        itemType: number;
        data: number[];
        betNum: number;
        winNum: number;
    }

    // Class defining the data received from call to proxy
    class InputHistoryTrans {
        isOpenDraw: boolean;
        dataTxId: string; // Only used for payouts
        txid: string;
        fromAddress: string;
        toAddress: string;
        quantity: number;
        assetType: string;
        date: number;
        data: any;

        constructor() {
            this.isOpenDraw = false;
            this.txid = '';
            this.fromAddress = '';
            this.toAddress = '';
            this.quantity = 0;
            this.assetType = '';
            this.date = 0;
        }
    }

    export function getbethistory(): HistoryData[] {
        var res: HistoryData[] = [];

        try {
            // Get lottery history and user bet history from proxy
            var xmlHttp = new XMLHttpRequest();
            //TODO switch to async and use callbacks
            xmlHttp.open("GET", "http://localhost:8081/bbApi/history", false);
            xmlHttp.send(null);
            let historyRes = JSON.parse(xmlHttp.responseText);

            // Keep an array of all round entries and draw entries
            let transactions: InputHistoryTrans[] = historyRes["@lottery"].hist;
            // Filter rounds to only include table 0
            let rounds: InputHistoryTrans[] = transactions.filter(item => item.data.r && item.data.r.length > 1 && item.data.r[1] === 0).sort((a, b) => a.date - b.date) // Sort ascending
            let draws: InputHistoryTrans[] = transactions.filter(item => item.data.d).sort((a, b) => a.date - b.date); // Sort ascending
            let payouts: InputHistoryTrans[] = transactions.filter(item => item.data.p);

            // Convert the associated bet txid for each payout for faster searching
            for (let pay of payouts) {
                // Validate that p array has both bet txid and winning amount, so at least 2 items in array
                if (pay.data.p && pay.data.p.length >= 2) {
                    pay.dataTxId = toHexString(pay.data.p[0].data);
                }
            }

            // Add a draw to the bottom of the array to represent the active lottery.  We are building an object that 
            // simulates the same data coming from the proxy so that 
            let openDraw: InputHistoryTrans = new InputHistoryTrans();
            // Use the most recent round for the open drawing, this is assumed to be the open round
            let recentRoundDate = getDateValueInSeconds(rounds[rounds.length - 1].data.r[0]);
            openDraw.date = recentRoundDate;
            console.log('Current open round data.r date', recentRoundDate);
            openDraw.isOpenDraw = true;
            draws.push(openDraw);

            // Loop through the array of lottery drawings sorted by most recent first
            let lastValidRoundIdx: number = -1;
            for (let draw of draws) {
                let currentDraw: any = {};
                let drawDate;

                // Validation on draw item to make sure it isn't test entry
                if (!draw.isOpenDraw && (!draw.data.d || draw.data.d.length < 2 || !draw.data.d[0].data)) {
                    console.log('Invalid draw entry, skipping', draw.txid);
                    continue;
                }

                // Set the draw number, aka phase
                if(draw.isOpenDraw && rounds[rounds.length - 1].data.r && rounds[rounds.length - 1].data.r.length > 2)
                    currentDraw.phase = rounds[rounds.length - 1].data.r[2];
                else if (draw.data && draw.data.d && draw.data.d.length >= 3)
                    currentDraw.phase = draw.data.d[2];
                else
                    currentDraw.phase = '';

                drawDate = new Date(draw.date * 1000);
                // Used for sorting
                currentDraw.drawDate = drawDate;
                // Set the text version of date/time for UI
                currentDraw.time = drawDate.getFullYear() + '/' + ('0' + (drawDate.getMonth() + 1)).slice(-2) + '/' + ('0' + drawDate.getDate()).slice(-2) + '/' + ('0' + drawDate.getHours()).slice(-2) + '/' + ('0' + drawDate.getMinutes()).slice(-2);

                // The open drawing doesn't have lottery results
                if (draw.isOpenDraw)
                    currentDraw.lotteryNum = [];
                else {
                    // Convert to number array if necessary
                    currentDraw.lotteryNum = convertToNumArray(draw.data.d[1]);
                }

                currentDraw.dataDetail = [];

                // Find the round associated with the drawing
                let roundIdx = -1;
                if (draw.isOpenDraw) {
                    // Use the most recent round for the open drawing, this is assumed to be the open round
                    roundIdx = rounds.length - 1;
                    console.log('Processing the currently open drawing that closes at ' + currentDraw.time, draw.date);
                }
                else {
                    // Find the round for this drawing
                    let drawTxid = toHexString(draw.data.d[0].data);
                    roundIdx = rounds.findIndex(item => item.txid === drawTxid);
                    console.log('Drawing for ' + currentDraw.time + ', winning numbers: ' + currentDraw.lotteryNum, draw.date, drawTxid);
                }

                // Add bets to the current drawing item
                if (roundIdx != -1) {
                    // Do a validation on the round data's date, should be > 6/1/2018
                    if (getDateValueInSeconds(rounds[roundIdx].data.r[0]) < 1527811200) {
                        console.log(`Skipping invalid draw b/c associated round date invalid.  Draw txid ${draw.txid}, round txid ${rounds[roundIdx].txid}`);
                        continue;
                    }

                    let bets: InputHistoryTrans[];
                    // The bets that belong to a drawing are all bet items that were placed before the closing time of 
                    // the round and after the closing time of the previous/earlier round

                    // Assume all times are in seconds
                    let closeRoundDate: number = getDateValueInSeconds(rounds[roundIdx].data.r[0]);
                    console.log('Associated round for drawing with date', closeRoundDate);

                    let prevRoundDate: number = -1;
                    if (lastValidRoundIdx === -1) {
                        bets = transactions.filter(item => item.data.b && ((item.date) <= closeRoundDate));
                    } else {
                        prevRoundDate = getDateValueInSeconds(rounds[lastValidRoundIdx].data.r[0]);
                        bets = transactions.filter(item => item.data.b && ((item.date) > prevRoundDate) && ((item.date) <= closeRoundDate));
                    }

                    console.log(`${bets.length} bets found between ${closeRoundDate} ${prevRoundDate}`, bets);

                    // Build array of user bet data for this lottery result
                    for (let item of bets) {
                        let dataG = [];
                        // The data array is only present for the Zodiac game type (2).  For this case the itemType is not
                        // included in the raw data array of the tx
                        // Notes: 
                        //   data.b[0] is the agent wallet address
                        //   data.b[1] is game type, data.b[2] is itemType (even for Zodiac game)
                        if (item.data.b.length > 3) {
                            dataG = item.data.b.slice(3);
                        }

                        let hDetail: any = {};
                        let winnings: number = 0;
                        let hDate = new Date(item.date * 1000);
                        hDetail.time = hDate.getFullYear() + '/' + ('0' + (hDate.getMonth() + 1)).slice(-2) + '/' + ('0' + hDate.getDate()).slice(-2) + '/' + ('0' + hDate.getHours()).slice(-2) + '/' + ('0' + hDate.getMinutes()).slice(-2);
                        //hDetail.time = moment(item.date).format('YYYY/MM/DD/hh/mm');

                        // Find the winnings for this bet
                        let betPayout = payouts.find(p => p.dataTxId != '' && p.dataTxId === item.txid);
                        if (betPayout) {
                            winnings = betPayout.data.p[1];
                            winnings = winnings / 100;
                            console.log(`Payout found for bet ${item.txid}: win ${betPayout.data.p[1]}, payout txid: ${betPayout.txid}`);
                        }

                        hDetail.betData = [
                            {
                                type: item.data.b[1], // Game type
                                itemType: item.data.b[2], // Item type
                                data: dataG,
                                betNum: item.quantity,
                                winNum: winnings
                            }
                        ];

                        currentDraw.dataDetail.push(hDetail);
                    }
                    // Remember the last valid round
                    lastValidRoundIdx = roundIdx;
                }

                // Add the drawing to the results for the game UI
                res.push(currentDraw);
            }

        }
        catch (e) {
            console.log('Exception retreiving history', e);
        }

        // Sort results descending to show most recent first
        res = res.sort((a, b) => b.drawDate - a.drawDate); // Sort descending
        //console.log('getbethistory returning response', res);
        return res;
    }

    export function getLotteryConfig(cb: Function) {
        // httpGet('http://localhost:8081/bbApi/getlotteryconfig', res => { cb(res); });
        setTimeout(() => { cb({maxBet: 10, minBet: 5}) }, 3000);
    }

    // function httpGet(url, cb) {
    //     var xhr = new XMLHttpRequest();
    //     xhr.open("GET", url, true);
    //     xhr.onload = function (e) {
    //         if (xhr.readyState === 4) {
    //             if (xhr.status === 200) {
    //                 cb(xhr.responseText);
    //             } else {
    //                 console.error(`http get error, url: ${url}, error: ${xhr.statusText}`);
    //             }
    //         }
    //     };
    //     xhr.onerror = function (e) {
    //         console.error(`http get error, url: ${url}, error: ${xhr.statusText}`);
    //     };
    //     xhr.send(null);
    // }

    function toHexString(byteArray: number[]) {
        return Array.from(byteArray, function (byte: number) {
            return ('0' + (byte & 0xFF).toString(16)).slice(-2);
        }).join('')
    }

    function convertToNumArray(obj: any) {
        let res: number[] = [];

        if (Object.prototype.toString.call(obj) === '[object Array]') {
            res = obj;
        }
        else if (typeof obj === 'string') {
            let stringNums = obj.split(/[\[,\]]+/); // Skip brackets and commas
            for (let s of stringNums) {
                if (s.length > 0)
                    res.push(parseInt(s));
            }
        }

        return res;
    }

    function getActiveRound(): InputHistoryTrans{
        try {
            // Get lottery history and user bet history from proxy
            var xmlHttp = new XMLHttpRequest();
            //TODO switch to async and use callbacks
            xmlHttp.open("GET", "http://localhost:8081/bbApi/history", false);
            xmlHttp.send(null);
            let historyRes = JSON.parse(xmlHttp.responseText);

            // Get the transaction data
            let transactions: InputHistoryTrans[] = historyRes["@lottery"].hist;
            let rounds: InputHistoryTrans[] = transactions.filter(item => item.data.r && item.data.r.length > 1 && item.data.r[1] === 0).sort((a, b) => b.date - a.date) // Sort descending

            // Use the time from the most recent round as the actively open lottery end time
            if (rounds.length > 0) {
                return rounds[0];
            }
        }
        catch (e) {
            console.log('Exception finding latest round data time', e);
        }

        return null;
    }

    /* Return the value converted to an integer in seconds
    */
    function getDateValueInSeconds(value: any): number {
        let intValue: number;
        intValue = parseInt(value);

        // If the value contains ms convert to seconds
        if (value > 1000000000000)
            return intValue / 1000;
        else
            return intValue;
    }

}

