class GameLobby extends eui.Component implements  eui.UIComponent {
	private itemList:eui.List;
	private arrayCollection: eui.ArrayCollection;
	private allBetNum:eui.Label;
	private submit:eui.Image;
	public constructor() {
		super();
		if(Game1.clientHeight/Game1.clientWidth > Game1Config.height/Game1Config.width){
			let scale:number=Game1.clientWidth/Game1Config.width
			this.scaleX=scale;
			this.scaleY=scale;
			this.height=Game1.clientHeight/scale;
		}else{
			let scale:number=Game1.clientHeight/Game1Config.height
			this.scaleX=scale;
			this.scaleY=scale;
			this.width=Game1.clientWidth/scale;
		}
		
	}

	protected partAdded(partName:string,instance:any):void
	{
		super.partAdded(partName,instance);
	}
	protected childrenCreated():void
	{
		super.childrenCreated();
		this.createUI();
	}
	public createUI(): void {
		let data: GameLobbyItemData[] = [];
		for (let i = 0; i < 7; i++) {
			data.push(new GameLobbyItemData(i as Game1Type));
		}
		this.arrayCollection = new eui.ArrayCollection(data);
		this.itemList.itemRenderer = GameLobbyItem;
		this.itemList.dataProvider = this.arrayCollection;
		this.arrayCollection.refresh();
		this.itemList.addEventListener(eui.ItemTapEvent.ITEM_TAP, this.onTouchItem, this)
		this.addEventListener(UIComponentEvent.UPDATEUI,this.onUpdateUI,this);
		this.submit.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onTouchSubmit, this);
		this.allBetNum.text = Util.numToShow(0);
	}
	public updateUI():void{
		for(let i:number=0;i<this.numChildren;i++){
			this.getChildAt(i).dispatchEvent(new GameUIEvent(GameUIEvent.UPDATE_USERINFO));
		}
	}
	private onTouchSubmit(e: egret.TouchEvent): void {
		UIManager.showSettlementUI();
	}
	public onUpdateUI(){
		let data: GameLobbyItemData[] = [];
		for (let i = 0; i < 7; i++) {
			data.push(new GameLobbyItemData(i as Game1Type));
			data[i].betNum=Game1.getSettlementManager().getBetNum(i);;
		}
		this.arrayCollection.replaceAll(data);
		this.allBetNum.text = Util.numToShow(Game1.getSettlementManager().getAllBet());
	}
	public onTouchItem(e:eui.ItemTapEvent):void{
		this.addChild(UIManager.getGameUI(e.itemIndex));
	}
}