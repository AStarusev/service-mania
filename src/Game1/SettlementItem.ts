class SettlementItem extends eui.ItemRenderer implements  eui.UIComponent {
	private closeBtn:eui.Image;
	private winNum:eui.Label;
	private betNum:eui.Label;
	public constructor() {
		super();
	}

	protected partAdded(partName:string,instance:any):void
	{
		super.partAdded(partName,instance);
	}


	protected childrenCreated():void
	{
		super.childrenCreated();
		this.closeBtn.addEventListener(egret.TouchEvent.TOUCH_TAP,this.onTouchClose,this);
	}
	private onTouchClose(e:egret.TouchEvent){
		this.dispatchEvent(new UIComponentEvent(UIComponentEvent.REMOVE_ITEM,true));
	}
	public dataChanged():void{
		this.winNum.text=Util.numToShow(this.data.winNum, 2);
		this.betNum.text=Util.numToShow(this.data.betNum, 2);
	}
	
}