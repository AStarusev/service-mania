class WaveSizeItem extends UIItemRenderer {
	public constructor() {
		super();
	}

	protected partAdded(partName: string, instance: any): void {
		super.partAdded(partName, instance);
	}


	protected childrenCreated(): void {

		super.childrenCreated();
		this.setSymbolGroupLayout( new MiddleTableLayout(5, -2, 0, 50, 50));
	}
	
}