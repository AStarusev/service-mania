class OddEvenItemData{
	public unSelectedSource: string;
    public selectedSource: string;
	public symbolData:Array<SymbolItemData>;
	public type:OddEvenType;
	public isShowBg:boolean;
	public constructor() {
	}
	public setData(type:OddEvenType){
		this.type=type;
		let symbols:SpecialCodeIFS[]=Game1.getOddEvenData(type);
		this.symbolData=[];
		for(let item of symbols){
			let symboldata:SymbolItemData=new SymbolItemData();
			symboldata.setData(item);
			this.symbolData.push(symboldata);
		}
		let title:string[]=["oddEven_odd","oddEven_even","oddEven_big","oddEven_small","oddEven_sumOdd","oddEven_sumEven"];
		this.unSelectedSource=title[type]+"_png";
    	this.selectedSource=title[type]+"_select"+"_png";
		this.isShowBg=false;
		if(type%2==0){
			this.isShowBg=true;
		}
	}

}
enum OddEvenType{
	Odd,Even,Big,Small,CombinedOdd,CombinedEven
}	