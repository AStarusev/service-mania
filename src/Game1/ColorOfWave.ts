class ColorOfWave extends UIComponent  {
	private arrayCollection: eui.ArrayCollection;
	public constructor() {
		super();
		
	}

	protected partAdded(partName: string, instance: any): void {
		super.partAdded(partName, instance);
	}
	protected childrenCreated(): void {
		super.childrenCreated();
		this.createUI();
	}
	public createUI(): void {
		let data: ColorOfWaveItemData[] = [];
		for (let i = 0; i < 3; i++) {
			data.push(new ColorOfWaveItemData(i as ColorOfWaveType));
		}
		this.arrayCollection = new eui.ArrayCollection(data);
		this.uiList.itemRenderer = ColorOfWaveItem;
		this.uiList.dataProvider = this.arrayCollection;
		this.arrayCollection.refresh();
	}

}