class NormalNumber extends UIComponent {
	private arrayCollection: eui.ArrayCollection;
	public constructor() {
		super();
		
	}

	protected partAdded(partName: string, instance: any): void {
		super.partAdded(partName, instance);
	}
	protected childrenCreated(): void {
		super.childrenCreated();
		this.createUI();
	}
	public createUI(): void {
		let data: ZodiacItemData[] = [];
		for (let i = 0; i < 12; i++) {
			data.push(new ZodiacItemData(i as ZodiacType));
		}
		this.arrayCollection = new eui.ArrayCollection(data);
		this.uiList.itemRenderer = ZodiacItem;
		this.uiList.dataProvider = this.arrayCollection;
		this.arrayCollection.refresh();
	}
	public getItemName(itemType: number) {
		return "平码-" + (Game1Config.GameInfo ? Game1Config.GameInfo.zodiacName[itemType] : " ");
	}

}