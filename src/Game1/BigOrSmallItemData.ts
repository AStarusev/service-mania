class BigOrSmallItemData {
	public unSelectedSource: string;
	public selectedSource: string;
	public symbolData: Array<SymbolItemData>;
	public type: BigOrSmallType;
	public isShowBg: boolean;
	public constructor(type: BigOrSmallType = null) {
		if (type != null) {
			this.setData(type);
		}
	}
	public setData(type: BigOrSmallType) {
		this.type = type;
		let symbols: SpecialCodeIFS[] = Game1.getBigOrSmallData(type);
		this.symbolData = [];
		for (let item of symbols) {
			let symboldata: SymbolItemData = new SymbolItemData();
			symboldata.setData(item);
			this.symbolData.push(symboldata);
		}
		let title: string[] = ["bigOrSmall_oddBig", "bigOrSmall_oddSmall", "bigOrSmall_evenBig", "bigOrSmall_evenSmall"];
		this.unSelectedSource = title[type] + "_png";
		this.selectedSource = title[type] + "_select" + "_png";
		this.isShowBg = type % 2 == 0;
	}

}
enum BigOrSmallType {
	OddBig, OddSmall, EvenBig, EvenSmall
}	