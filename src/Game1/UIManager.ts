module UIManager {
	let uiPool:UIComponent[] = [];
	let settlementUI:eui.Component;
	let mainLayer:eui.UILayer;
	export let gameUINode:GameLobby;
	let settlementNode:egret.Sprite;
	let topNode:egret.Sprite;
	let historyNode:egret.Sprite;
	let history:HistoryUI;
	export function updateUI(){
		Game1.updateData();
		if(gameUINode){
			gameUINode.updateUI();
		}
	}
	export function showSettlementUI():void{
		if(!settlementUI){
			settlementUI=new Settlement();
		}
		settlementNode.removeChildren();
		settlementNode.addChild(settlementUI);
	}
	export function showPromptPanel(type:string):void{
		topNode.addChild(new PromptPanel(type));
	}
	export function removeChildren():void{
		topNode.removeChildren();
	}
	export function showHistory():void{
		if(!history){
			history=new HistoryUI();
		}else{
			history.updateUI();
		}
		historyNode.addChild(history);
	}
	export function setMainLayer(layer:eui.UILayer):void{
		mainLayer=layer;
		settlementNode=new egret.Sprite();
		topNode=new egret.Sprite();
		historyNode=new egret.Sprite();
		gameUINode=new GameLobby()
		layer.addChild(gameUINode);
		layer.addChild(historyNode);
		layer.addChild(settlementNode);
		layer.addChild(topNode);
	}
	export function getGameUI(type: Game1Type): UIComponent {
		let res:UIComponent;
		res = uiPool[type];
		if (res) {

		} else {
			switch (type) {
				case Game1Type.Symbol:
					res = new BetUI();
					break;
				case Game1Type.RangeOfColor:
					res = new ColorOfWave();
					break;
				case Game1Type.NormalNumber:
					res = new NormalNumber();
					break;
				case Game1Type.BigOrSmall:
					res = new BigOrSmall();
					break;
				case Game1Type.OddEven:
					res = new OddEven();
					break;
				case Game1Type.ColorSize:
					res = new WaveSize();
					break;
				case Game1Type.Zodiac:
					res = new Zodiac();
					break;
			}
			res.setGameType(type);
			uiPool[type]=res;
		}
		res.updateUI();
		return res;
	}

}