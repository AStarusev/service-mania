class PromptPanel extends eui.Panel implements  eui.UIComponent {
	public constructor(state:string) {
		super();
		if (Game1.clientHeight / Game1.clientWidth > Game1Config.height / Game1Config.width) {
			let scale: number = Game1.clientWidth / Game1Config.width
			this.scaleX = scale;
			this.scaleY = scale;
			this.height = Game1.clientHeight / scale;
		} else {
			let scale: number = Game1.clientHeight / Game1Config.height
			this.scaleX = scale;
			this.scaleY = scale;
			this.width = Game1.clientWidth / scale;
		}
		this.currentState=state;
	}

	protected partAdded(partName:string,instance:any):void
	{
		super.partAdded(partName,instance);
	}


	protected childrenCreated():void
	{
		super.childrenCreated();
	}
	
}