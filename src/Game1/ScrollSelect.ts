class ScrollSelect extends eui.Component implements eui.UIComponent {
	private touchGroup: eui.Group;
	private touchY: number;
	private pos: number;
	private list: eui.List;
	private arrayCollection: eui.ArrayCollection;
	public 
	public constructor() {
		super();
	}

	protected partAdded(partName: string, instance: any): void {
		super.partAdded(partName, instance);
	}


	protected childrenCreated(): void {
		super.childrenCreated();
		let data: string[] = [];
		for (let i = 0; i < 3; i++) {
			for (let j = 1; j <= 12; j++) {
				data.push(Util.numToString(j));
			}
		}
		this.arrayCollection = new eui.ArrayCollection(data);
		this.list.itemRenderer = eui.ItemRenderer;
		this.list.dataProvider = this.arrayCollection;
		this.list.height = 35 * this.arrayCollection.length;
		this.list.y = -35 * 12;
		//this.list.y=;
		this.arrayCollection.refresh();
		console.log(1111);
		this.pos = 0;
		this.touchGroup.addEventListener(egret.TouchEvent.TOUCH_BEGIN, (e: egret.TouchEvent) => {
			this.touchY = e.localY;
			console.log(this.touchY)
		}, this);
		this.touchGroup.addEventListener(egret.TouchEvent.TOUCH_MOVE, (e: egret.TouchEvent) => {
			if (e.localY - this.touchY > 25) {
				this.touchY = e.localY;
				this.pos++;
				egret.Tween.removeTweens(this.list);
				egret.Tween.get(this.list).to({ y: this.pos * 35 -35 * 12}, 200).call(() => {
					if ( this.list.y>-35*12 ){
						this.list.y-=35*12;
						this.pos-=12;
					}
					
				});
			}
			if (e.localY - this.touchY < -25) {
				this.touchY = e.localY;
				this.pos--;
				egret.Tween.removeTweens(this.list);
				egret.Tween.get(this.list).to({ y: this.pos * 35 -35 * 12}, 200).call(() => { 
					if ( this.list.y<-35*24 ){
						this.list.y+=35*12;
						this.pos+=12;
					}
				});
			}
		}, this);
	}

}