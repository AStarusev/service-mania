class BetUI extends UIComponent  {
	public constructor() {
		super();
		this.skinName = "BetUISkin";
		
	}
	public resetUI() {
		super.resetUI();
	}
	private balance:eui.Label; 
	protected partAdded(partName: string, instance: any): void {
		super.partAdded(partName, instance);
		//egret.superSetter();
	}
	protected childrenCreated(): void {
		super.childrenCreated();
		this.createUI();
	}
	protected getItemName(itemType: number) {
		return "特码 "+Util.numToString(itemType+1);
    }
	private createUI() : void{
		this.uiList.itemRenderer = SymbolItem;
		var collection: eui.ArrayCollection = new eui.ArrayCollection();
		var data: SymbolItemData[] = [];
		var dataSource: Array<SpecialCodeIFS> = Game1.getSpecialCodeData();
		for (var i = 0; i < dataSource.length; i++) {
			var symbolData:SymbolItemData=new SymbolItemData();
			symbolData.specialCode=Util.numToString(dataSource[i].specialCode);
			symbolData.setColorId(dataSource[i].colorId);
			data.push(symbolData);
		}
		collection.source = data;
		this.uiList.dataProvider = collection;
		collection.refresh();
	}

}
