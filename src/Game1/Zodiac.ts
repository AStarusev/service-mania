
class Zodiac extends UIComponent {
	private arrayCollection: eui.ArrayCollection;
	private zodiac_beasts: eui.RadioButton;
	private zodiac_poultry: eui.RadioButton;
	private zodiac_rat: eui.RadioButton;
	private zodiac_horse: eui.RadioButton;
	private zodiacRadioGroup: eui.RadioButtonGroup;
	private zodiacCategory: Array<Array<ZodiacType>>;
	private zodiacRadioList: eui.RadioButtonGroup[];
	public constructor() {
		super();
		
	}

	protected partAdded(partName: string, instance: any): void {
		super.partAdded(partName, instance);
	}
	protected childrenCreated(): void {
		super.childrenCreated();
		this.createUI();
	}
	public createUI(): void {
		let data: ZodiacItemData[] = [];
		for (let i = 0; i < 12; i++) {
			let itemData:ZodiacItemData=new ZodiacItemData(i as ZodiacType)
			itemData.symbolData.length=4;
			data.push(itemData);
		}
		this.arrayCollection = new eui.ArrayCollection(data);
		this.uiList.itemRenderer = ZodiacItem;
		this.uiList.dataProvider = this.arrayCollection;
		this.arrayCollection.refresh();

		this.zodiacCategory = new Array<Array<ZodiacType>>();
		this.zodiacCategory[ZodiacCategoryType.BeastZodiac] = [ZodiacType.Rat, ZodiacType.Tiger, ZodiacType.Rabbit, ZodiacType.Dragon, ZodiacType.Snake, ZodiacType.Monkey];
		this.zodiacCategory[ZodiacCategoryType.LivestockZodiac] = [ZodiacType.Ox, ZodiacType.Horse, ZodiacType.Goat, ZodiacType.Rooster, ZodiacType.Dog, ZodiacType.Pig];
		this.zodiacCategory[ZodiacCategoryType.RatSide] = [ZodiacType.Rat, ZodiacType.Ox, ZodiacType.Tiger, ZodiacType.Rabbit, ZodiacType.Dragon, ZodiacType.Snake];
		this.zodiacCategory[ZodiacCategoryType.HorseSide] = [ZodiacType.Horse, ZodiacType.Goat, ZodiacType.Monkey, ZodiacType.Rooster, ZodiacType.Dog, ZodiacType.Pig];

		this.zodiacRadioGroup = new eui.RadioButtonGroup();

		this.zodiac_beasts.group = this.zodiacRadioGroup;
		this.zodiac_poultry.group = this.zodiacRadioGroup;
		this.zodiac_rat.group = this.zodiacRadioGroup;
		this.zodiac_horse.group = this.zodiacRadioGroup;

		this.zodiacRadioGroup.addEventListener(eui.UIEvent.CHANGE, ():void => {
			this.touchRadioEvent(this.zodiacRadioGroup.selectedValue);
		}, this)
	}
	protected addUIListChangEvent(): void{
        this.uiList.addEventListener(egret.Event.CHANGE, this.CheckZodiacList, this);
    }
	public resetUI(): void {
        this.uiList.selectedIndex = -1;
		this.uiList.selectedIndices=[];
		this.zodiacRadioGroup.$setSelection(null, true);
    }
	private touchRadioEvent(type: ZodiacCategoryType): void {
		if (type != null) {		
			this.uiList.selectedIndices = this.zodiacCategory[type];
			this.selectChange(type,this.zodiacCategory[type] );
		}

	}
	private CheckZodiacList(): void {
		
		let zodiacCategoryType: ZodiacCategoryType = -1;
		
		if (this.uiList.selectedItems.length == 6) {
			zodiacCategoryType = ZodiacCategoryType.SixZodiacSigns;
			let typeList: number[] = [];
			for (let item of this.uiList.selectedItems as ZodiacItem[]) {
				typeList.push(item.type);
			}
			typeList = typeList.sort((x: number, y: number): number => {
				if (x < y) {
					return -1;
				} else if (x > y) {
					return 1;
				} else {
					return 0;
				}
			});
			let typeStr = typeList.join("_");
			for (let i: number = 0; i < 4; i++) {
				if (this.zodiacCategory[i].join("_") == typeStr) {
					zodiacCategoryType = i as ZodiacCategoryType;
					break;
				}
			}
		} else {
			switch (this.uiList.selectedItems.length) {
				case 1:
					zodiacCategoryType = ZodiacCategoryType.OneZodiacSign;
					break;
				case 3:
					zodiacCategoryType = ZodiacCategoryType.ThreeZodiacSigns;
					break;
				case 4:
					zodiacCategoryType = ZodiacCategoryType.FourZodiacSigns;
					break;
				case 5:
					zodiacCategoryType = ZodiacCategoryType.FiveZodiacSigns;
					break;
				case 6:
					zodiacCategoryType = ZodiacCategoryType.SixZodiacSigns;
					break;
			}
		}		
		this.setSelectRadio(zodiacCategoryType as ZodiacCategoryType);
		let res:number[]=[];
		for (let item of this.uiList.selectedItems as ZodiacItem[]) {
			res.push(item.type);
		}
		res = res.sort((x: number, y: number): number => {
			if (x < y) {
				return -1;
			} else if (x > y) {
				return 1;
			} else {
				return 0;
			}
		});
		this.selectChange(zodiacCategoryType as ZodiacCategoryType,res);
	}
	public getItemName(itemType: number) :string{
		if(itemType<4){
			return Game1Config.GameInfo ? Game1Config.GameInfo.gameNameConfig[this.gameType].betTypeName[itemType] : " ";
		}else{
			let res:number[]=[];
			for (let item of this.uiList.selectedItems as ZodiacItem[]) {
				res.push(item.type);
			}
			res = res.sort((x: number, y: number): number => {
				if (x < y) {
					return -1;
				} else if (x > y) {
					return 1;
				} else {
					return 0;
				}
			});
			let resStr:string[]=[];
			for (let item of res) {
				resStr.push(Game1Config.GameInfo.zodiacName[item]);
			}
			return resStr.join(",");
		}
        
    }
	public setSelectRadio(type: ZodiacCategoryType): void {

		if (type <= ZodiacCategoryType.HorseSide) {
			this.zodiacRadioGroup.$setSelection(this.zodiacRadioGroup.getRadioButtonAt(type), true);
		} else {
			this.zodiacRadioGroup.$setSelection(null, true);
		}

	}
}
 enum ZodiacCategoryType {
        BeastZodiac, LivestockZodiac, RatSide, HorseSide, OneZodiacSign, ThreeZodiacSigns, FourZodiacSigns, FiveZodiacSigns, SixZodiacSigns
     }
