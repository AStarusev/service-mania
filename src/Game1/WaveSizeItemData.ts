class WaveSizeItemData {
	
	public unSelectedSource: string;
    public selectedSource: string;
	public frameSource: string;
    public frameBgSource: string;
	public symbolData:Array<SymbolItemData>;
	public type:WaveSizeType;
	public isShowBg:boolean;
	public constructor(type:WaveSizeType=null) {
		if(type!=null){
			this.setData(type);
		}
	}
	public setData(type:WaveSizeType){
		this.type=type;
		let symbols:SpecialCodeIFS[]=Game1.getWaveSizeData(type);
		this.symbolData=[];
		for(let item of symbols){
			let symboldata:SymbolItemData=new SymbolItemData();
			symboldata.setData(item);
			this.symbolData.push(symboldata);
		}
		let color:string[]=["red","blue","green"];
		let title:string[]=["colorSize_redSmall","colorSize_redBig","colorSize_blueSmall","colorSize_blueBig","colorSize_greenSmall","colorSize_greenBig"];
		this.frameSource="colorSize_frame_"+color[Math.floor(type/2)]+"_png";
		this.frameBgSource="colorSize_frameBg_"+color[Math.floor(type/2)]+"_png";
		this.unSelectedSource= Util.joinString("_", "colorSize", WaveSizeType[type], "png");
    	this.selectedSource=Util.joinString("_", "colorSize", WaveSizeType[type],"select", "png")
		this.isShowBg=type%2==0?true:false;
	}
}
enum WaveSizeType{
	RedSmall,RedBig,BlueSmall,BlueBig,GreenSmall,GreenBig
}