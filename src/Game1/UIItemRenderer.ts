class UIItemRenderer extends eui.ItemRenderer implements eui.UIComponent {
	private imgBg: eui.Image;
	private imgTitle_UnSelected: eui.Image;
	private imgTitle_Selected: eui.Image;
	private symbolList: eui.List;
	private symbolGroup: eui.Group;
	private arrayCollection: eui.ArrayCollection;
	public constructor() {
		super();
	}
	protected partAdded(partName: string, instance: any): void {
		super.partAdded(partName, instance);
	}
	protected childrenCreated(): void {
		super.childrenCreated();
	}
	protected dataChanged(): void {
		if (this.symbolList) {
			if (!this.arrayCollection) {
				this.arrayCollection = new eui.ArrayCollection();
				this.symbolList.dataProvider = this.arrayCollection;
				this.symbolList.itemRenderer = SymbolItem;
			}
			this.arrayCollection.source = this.data["symbolData"];
			this.arrayCollection.refresh();
		} else {
			this.symbolGroup.removeChildren();
			for (let i = 0; i < this.data["symbolData"].length; i++) {
				let symbol = new SymbolItem();
				symbol.setData(this.data["symbolData"][i]);
				this.symbolGroup.addChild(symbol);
			}
		}

	}
	public setSymbolGroupLayout(layout:eui.LayoutBase):void{
		this.symbolGroup.layout = layout;
		this.symbolGroup.layout.measure();
	}
	public invalidateState():void {
		super.invalidateState();
		switch (this.currentState) {
			case "up":
				this.setSelect(false);
				break;
			case "down":
				this.setSelect(true);
				break;
			case "upAndSelected":
				this.setSelect(true);
				break;
		}
	}
	private setSelect(isSelect: boolean):void {
		let symbolUI:eui.Group=this.symbolList?this.symbolList:this.symbolGroup;
		for (let itemIndex = 0; itemIndex < symbolUI.numElements; itemIndex++) {
			let item: SymbolItem = <SymbolItem>(symbolUI.getElementAt(itemIndex));
			if (egret.is(item, "SymbolItem")) {
				item.selected = isSelect;
			}
		}
	}
	
	public get type():number{
		return Number(this.data["type"]);
	}
}