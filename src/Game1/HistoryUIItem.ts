class HistoryUIItem extends eui.ItemRenderer implements eui.UIComponent {
	private touchGroup: eui.Group;
	private arrayCollection: eui.ArrayCollection;
	private arrayCollection_symbol: eui.ArrayCollection;
	private symbolGroup:eui.Group;
	private symbolTipLabel:eui.Label;
	private infoList: eui.List;
	private symbolList: eui.List;
	private tipGroup: eui.Group;
	private symbol: SymbolItem;
	private phase: eui.Label;
	private time: eui.Label;
	public constructor() {
		super();
	}

	protected partAdded(partName: string, instance: any): void {
		super.partAdded(partName, instance);
	}


	protected childrenCreated(): void {
		super.childrenCreated();
		this.createUI();
	}
	private createUI(): void {
		this.arrayCollection_symbol = new eui.ArrayCollection([]);
		this.symbolList.itemRenderer = SymbolItem;
		this.symbolList.dataProvider = this.arrayCollection_symbol;
		this.touchGroup.addEventListener(egret.TouchEvent.TOUCH_TAP, this.touchThis, this);
	}
	protected dataChanged() {
		this.currentState = this.data["currentState"];
		if(this.currentState=="selected"){
			this.updateInfo();
		}
		let thisData: HistoryUIItemData = this.data as HistoryUIItemData;
		let data: SymbolItemData[] = [];
		if (thisData.specialCode ){
			this.symbolGroup.visible=true;
			this.symbolTipLabel.visible=false;
			for (let i in thisData.specialCodeList) {
				data.push(new SymbolItemData({
					specialCode: thisData.specialCodeList[i],
					colorId: Game1.getColorIdByCode(thisData.specialCodeList[i])
				}));
			}
			this.arrayCollection_symbol.source = data;
			this.symbol.setData(new SymbolItemData({
				specialCode: thisData.specialCode,
				colorId: Game1.getColorIdByCode(thisData.specialCode)
			}));
			//this.updateInfo();
		}else{
			this.symbolGroup.visible=false;
			this.symbolTipLabel.visible=true;
		}
		this.phase.text = thisData.phase;
		this.time.text = thisData.time;
	}
	public touchThis(e: egret.Event) {
		this.currentState = this.currentState == "unSelected" ? "selected" : "unSelected";
		this.data["currentState"] = this.currentState;
		this.parent["validateDisplayList"]();
	}
	public invalidateState(): void {
		super.invalidateState();
		switch (this.currentState) {
			case "unSelected":
				this.height = 72;
				this.infoList.visible = false;
				this.tipGroup.visible = false;
				break;
			case "selected":
				this.infoList.visible = true;
				this.updateInfo();
				break;
		}

	}
	private updateInfo() {
		let thisData: HistoryUIItemData = this.data as HistoryUIItemData;
		this.tipGroup.visible = false;
		if (thisData.betData.length == 0) {
			this.infoList.visible = false;
			this.tipGroup.visible = true;
			this.height = 72 + 26;
			return;
		}
		this.infoList.visible = true;
		this.height = 72 + thisData.betData.length * 58;
		this.infoList.height = thisData.betData.length * 58;
		//let data: HistoryUIItemData[] = [];
		if (!this.arrayCollection) {
			this.arrayCollection = new eui.ArrayCollection(thisData.betData);
			this.infoList.itemRenderer = HistoryUIInfoItem;
			this.infoList.dataProvider = this.arrayCollection;
		}
		this.arrayCollection.source = thisData.betData;
		this.arrayCollection.refresh();
	}
}