class BetInput extends eui.TextInput implements eui.UIComponent {
	public constructor() {
		super();
	}
	protected partAdded(partName: string, instance: any): void {
		super.partAdded(partName, instance);
	}
	protected childrenCreated(): void {
		super.childrenCreated();
		this.textDisplay.text=Util.numToShow(0);
		this.textDisplay.addEventListener(egret.FocusEvent.FOCUS_IN, function (e: egret.FocusEvent) {
			this.textDisplay.text = this.textDisplay.text.replace(Game1.moneyType, "");
			this.textDisplay.text = this.textDisplay.text == "0" ? "" : this.textDisplay.text;
		}, this);
		this.textDisplay.addEventListener(egret.FocusEvent.FOCUS_OUT, function (e: egret.FocusEvent) {
			this.textDisplay.text = Util.numToShow( (this.textDisplay.text == "" ? "0" : Number(this.textDisplay.text).toString()));
			this.parent.dispatchEvent(new GameUIEvent(GameUIEvent.UPDATE, true));
		}, this);
		this.textDisplay.addEventListener(egret.Event.CHANGE, function (e: egret.Event) {
			if (Number(this.textDisplay.text.replace(Game1.moneyType, "")) > Number(NetManager.getbalance())) {
				this.textDisplay.text = NetManager.getbalance().toString();
			}
		}, this);
	}
	public getInputNum(): number {
		let res: string = this.textDisplay.text ? this.textDisplay.text.replace(/[^0-9]/g, "") : "0";
		return Number(res);
	}

}