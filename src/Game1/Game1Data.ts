interface SpecialCodeIFS {
	specialCode: number;
	colorId: SymbolColor;
}
enum SymbolColor {
	Red, Blue, Green
}
enum Game1Type {
	Symbol, OddEven, Zodiac, BigOrSmall, RangeOfColor, ColorSize, NormalNumber
}
module Game1 {
	let specialCodeData: Array<SpecialCodeIFS> = [
	];
	let zodiacData: Array<Array<SpecialCodeIFS>> = [
	];
	export let moneyType:string="$";
	export let time:string;
	export let phase:string;
	export let clientHeight:number;
	export let clientWidth:number;
	export let paytable:number[][]=[
		[1],
		[1,1,1,1,1,1],
		[1,2,3,4,5,6,7,8,9],
		[1,1,1,1],
		[1.3,1,1],
		[1,2,3,4,5,6],
		[1,1,1,1,1,1,1,1,1,1,1.3,1],
	];
	export let lotteryConfig: Object;
	function initSpecialCodeData(): void {
		for (let i = 0; i < 49; i++) {
			let colorId = parseInt((parseInt(i / 10 + '') + i) / 2 + '') % 3;
			specialCodeData.push({ specialCode: i + 1, colorId: colorId as SymbolColor });
		}
	}
	export function getColorIdByCode(code:number):number{
		code-=1;
		return parseInt((parseInt(code / 10 + '') + code) / 2 + '') % 3;;
	}
	export function initData(): void {
		moneyType=NetManager.getbalancetype();
		setInterval(setGameTime, 15 * 1000);
		setGameTime();
		phase=NetManager.getphase();
		paytable=NetManager.getreturnrate();
		initSpecialCodeData();
		initZodiacData();
		clientHeight=document.documentElement.clientHeight;
		clientWidth=document.documentElement.clientWidth;

		NetManager.getLotteryConfig(c => { lotteryConfig = c });
	}

	export function updateData(){
		moneyType=NetManager.getbalancetype();
		setGameTime();
		phase=NetManager.getphase();
		paytable=NetManager.getreturnrate();
	}

	function setGameTime(): void{
		let tmpTime = NetManager.getlotteryendtime();
		if(tmpTime && tmpTime.length > 0){
			let str:string[] = tmpTime.split("/");
			time = str[0]+"/"+str[1]+"/"+str[2]+" "+str[3]+":"+str[4];
		} else {
			time = '';
		}
	}

	function initZodiacData(): void {
		let year: number = NetManager.getyear();
		let zodiac: ZodiacType = (year + 8) % 12;
		for (let symbol of specialCodeData) {
			if (!zodiacData[zodiac]) {
				zodiacData[zodiac] = [];
			}
			zodiacData[zodiac].push(symbol);
			zodiac=zodiac==0?11:zodiac-1;
		}
	}
	export function getSpecialCodeData(): SpecialCodeIFS[] {
		return specialCodeData;
	}
	export function getOddEvenData(type: OddEvenType): SpecialCodeIFS[] {
		let specialCodeRes: Array<SpecialCodeIFS> = [];
		for (let symbol of specialCodeData) {
			switch (type) {
				case OddEvenType.Odd:
					if (symbol.specialCode % 2 != 0 && symbol.specialCode != 49) {
						specialCodeRes.push(symbol);
					}
					break;
				case OddEvenType.Even:
					if (symbol.specialCode % 2 == 0 && symbol.specialCode != 49) {
						specialCodeRes.push(symbol);
					}
					break;
				case OddEvenType.Big:
					if (symbol.specialCode > 24 && symbol.specialCode != 49) {
						specialCodeRes.push(symbol);
					}
					break;
				case OddEvenType.Small:
					if (symbol.specialCode <= 24 && symbol.specialCode != 49) {
						specialCodeRes.push(symbol);
					}
					break;
				case OddEvenType.CombinedOdd:
					if ((Math.floor(symbol.specialCode / 10) + symbol.specialCode % 10) % 2 != 0 && symbol.specialCode != 49) {
						specialCodeRes.push(symbol);
					}
					break;
				case OddEvenType.CombinedEven:
					if ((Math.floor(symbol.specialCode / 10) + symbol.specialCode % 10) % 2 == 0 && symbol.specialCode != 49) {
						specialCodeRes.push(symbol);
					}
					break;
			}

		}
		return specialCodeRes;
	}
	export function getColorOfWaveData(type: ColorOfWaveType): SpecialCodeIFS[] {
		let specialCodeRes: Array<SpecialCodeIFS> = [];
		for (let symbol of specialCodeData) {
			switch (type) {
				case ColorOfWaveType.Red:
					if (symbol.colorId == SymbolColor.Red) {
						specialCodeRes.push(symbol);
					}
					break;
				case ColorOfWaveType.Blue:
					if (symbol.colorId == SymbolColor.Blue) {
						specialCodeRes.push(symbol);
					}
					break;
				case ColorOfWaveType.Green:
					if (symbol.colorId == SymbolColor.Green) {
						specialCodeRes.push(symbol);
					}
					break;
			}

		}
		return specialCodeRes;
	}
	export function getWaveSizeData(type: WaveSizeType): SpecialCodeIFS[] {
		let specialCodeRes: Array<SpecialCodeIFS> = [];
		for (let symbol of specialCodeData) {
			switch (type) {
				case WaveSizeType.RedSmall:
					if (symbol.colorId == SymbolColor.Red && symbol.specialCode <= 24) {
						specialCodeRes.push(symbol);
					}
					break;
				case WaveSizeType.RedBig:
					if (symbol.colorId == SymbolColor.Red && symbol.specialCode > 24 && symbol.specialCode != 49) {
						specialCodeRes.push(symbol);
					}
					break;
				case WaveSizeType.BlueSmall:
					if (symbol.colorId == SymbolColor.Blue && symbol.specialCode <= 24) {
						specialCodeRes.push(symbol);
					}
					break;
				case WaveSizeType.BlueBig:
					if (symbol.colorId == SymbolColor.Blue && symbol.specialCode > 24 && symbol.specialCode != 49) {
						specialCodeRes.push(symbol);
					}
					break;
				case WaveSizeType.GreenSmall:
					if (symbol.colorId == SymbolColor.Green && symbol.specialCode <= 24) {
						specialCodeRes.push(symbol);
					}
					break;
				case WaveSizeType.GreenBig:
					if (symbol.colorId == SymbolColor.Green && symbol.specialCode > 24 && symbol.specialCode != 49) {
						specialCodeRes.push(symbol);
					}
					break;
			}

		}
		return specialCodeRes;
	}

	export function getBigOrSmallData(type: BigOrSmallType): SpecialCodeIFS[] {
		let specialCodeRes: Array<SpecialCodeIFS> = [];
		for (let symbol of specialCodeData) {
			switch (type) {
				case BigOrSmallType.OddBig:
					if (symbol.specialCode % 2 != 0 && symbol.specialCode > 24 && symbol.specialCode != 49) {
						specialCodeRes.push(symbol);
					}
					break;
				case BigOrSmallType.OddSmall:
					if (symbol.specialCode % 2 != 0 && symbol.specialCode <= 24) {
						specialCodeRes.push(symbol);
					}
					break;
				case BigOrSmallType.EvenBig:
					if (symbol.specialCode % 2 == 0 && symbol.specialCode > 24 && symbol.specialCode != 49) {
						specialCodeRes.push(symbol);
					}
					break;
				case BigOrSmallType.EvenSmall:
					if (symbol.specialCode % 2 == 0 && symbol.specialCode <= 24) {
						specialCodeRes.push(symbol);
					}
					break;
			}
		}
		return specialCodeRes;
	}
	export function getZodiacData(type: ZodiacType): SpecialCodeIFS[] {
		return zodiacData[type];
	}
	

	
}