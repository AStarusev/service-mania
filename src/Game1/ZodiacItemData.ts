class ZodiacItemData {
	public unSelectedSource: string;
	public selectedSource: string;
	public symbolData: Array<SymbolItemData>;
	public type: ZodiacType;
	public constructor(type: ZodiacType = null) {
		if (type != null) {
			this.setData(type);
		}
	}
	public setData(type: ZodiacType) {
		this.type = type;
		let symbols: SpecialCodeIFS[] = Game1.getZodiacData(type);
		this.symbolData = [];
		for (let item of symbols) {
			let symboldata: SymbolItemData = new SymbolItemData();
			symboldata.setData(item);
			this.symbolData.push(symboldata);
		}
		this.unSelectedSource = Util.joinString("_", "zodiac", ZodiacType[type], "png");
		this.selectedSource = Util.joinString("_", "zodiac", ZodiacType[type], "select", "png");
	}

}

enum ZodiacType {
	/**鼠*/
	Rat,
	/**牛*/
	Ox,
	/**虎*/
	Tiger,
	/**兔*/
	Rabbit,
	/**龙*/
	Dragon,
	/**蛇*/
	Snake,
	/**马*/
	Horse,
	/**羊*/
	Goat,
	/**猴*/
	Monkey,
	/**鸡*/
	Rooster,
	/**狗*/
	Dog,
	/**猪*/
	Pig
}