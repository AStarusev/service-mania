class UIComponent extends eui.Component implements eui.UIComponent {
    public gameType: Game1Type;
    public gameUI: GameUI;
    public uiList: eui.List;
    public paytable: eui.Label;
    public paytable1: eui.Label;
    public constructor() {
        super();
        if (Game1.clientHeight / Game1.clientWidth > Game1Config.height / Game1Config.width) {
            let scale: number = Game1.clientWidth / Game1Config.width
            this.height = Game1.clientHeight / scale;
        } else {
            let scale: number = Game1.clientHeight / Game1Config.height
            this.width = Game1.clientWidth / scale;
        }
    }

    protected childrenCreated(): void {
        super.childrenCreated();
        this.gameUI.setUIComponent(this);
        this.addUIListChangEvent();
        this.addEventListener(UIComponentEvent.RESET, this.resetUI, this);
        this.setPaytable();
        this.addEventListener(UIComponentEvent.REMOV_UICOMPONENT,()=>{
            this.parent.removeChild(this);
        },this);
        this.addEventListener(GameUIEvent.UPDATE_USERINFO, () => { 
			this.gameUI.dispatchEvent(new GameUIEvent(GameUIEvent.UPDATE_USERINFO));
		}, this);
    }
    private setPaytable(): void {
        let paytable: number[] = Game1.paytable[this.gameType];
        for (let i = 0; i < paytable.length; i++) {
            if (this.paytable.text.indexOf("%" + i) != -1) {
                this.paytable.text = this.paytable.text.replace("%" + i, "" + paytable[i]);
            } else {
                if (this.paytable1) {
                    if (this.paytable1.text.indexOf("%" + i) != -1) {
                        this.paytable1.text = this.paytable1.text.replace("%" + i, "" + paytable[i]);
                    }
                }
            }
        }
    }
    protected addUIListChangEvent(): void {
        this.uiList.addEventListener(egret.Event.CHANGE, () => {
            this.selectChange(this.uiList.selectedIndex);
        }, this);
    }
    public get gameNameStr(): string {
        return Game1Config.GameInfo ? Game1Config.GameInfo.gameNameConfig[this.gameType].name : " ";
    }
    public setGameType(type: Game1Type): void {
        this.gameType = type;
    }
    protected resetUI(): void {
        this.uiList.selectedIndex = -1;
    }
    protected getItemName(itemType: number): string {
        return Game1Config.GameInfo ? Game1Config.GameInfo.gameNameConfig[this.gameType].betTypeName[itemType] : " ";
    }
    public selectChange(itemType: number, data: any = null): void {
        let event: GameUIEvent = new GameUIEvent(GameUIEvent.UPDATE);
        event.itemType = Number(itemType);
        event.data = data;
        event.name = this.getItemName(itemType);
        this.gameUI.dispatchEvent(event);
    }
    public updateUI():void{
        if(this.gameUI){
            this.gameUI.dispatchEvent(new GameUIEvent(GameUIEvent.UPDATE_USERINFO));
        }     
        if (Game1.clientHeight / Game1.clientWidth > Game1Config.height / Game1Config.width) {
            let scale: number = Game1.clientWidth / Game1Config.width
            this.height = Game1.clientHeight / scale;
        } else {
            let scale: number = Game1.clientHeight / Game1Config.height
            this.width = Game1.clientWidth / scale;
        }
    }
}
class UIComponentEvent extends egret.Event {
    public static UPDATEUI: string = "UpdateUI";
    public static RESET: string = "Reset";
    public static REMOVE_ITEM: string = "RemoveItem";
    public static REMOV_UICOMPONENT: string = "RemoveUIComponent";
    public data: any;
    public constructor(type: string, bubbles: boolean = false, cancelable: boolean = false) {
        super(type, bubbles, cancelable);
    }
}