class HistoryUIItemData {
	public currentState: string;
	public time: string;
	public phase: string;
	public specialCodeList: number[];
	public specialCode: number;
	public betData: HistoryUIInfoItemData[];
	public constructor() {
		this.currentState = "unSelected";
	}
	public setData(data: NetManager.HistoryData): void {
		this.currentState = "unSelected";
		let str: string[] = data.time.split("/");
		this.time = str[0] + "/" + str[1] + "/" + str[2] + " " + str[3] + ":" + str[4];
		this.phase = data.phase;
		this.specialCode = data.lotteryNum[6];
		this.specialCodeList = data.lotteryNum;	
		this.specialCodeList.length = 6;
		
		this.betData = [];
		for (let i = 0; i < data.dataDetail.length; i++) {
			let betData: HistoryUIInfoItemData[] = [];
			for (let j = 0; j < data.dataDetail[i].betData.length; j++) {
				let itemData: HistoryUIInfoItemData = new HistoryUIInfoItemData();
				itemData.setData(data.dataDetail[i].betData[j]);
				betData.push(itemData);
			}
			betData = betData.sort((x: HistoryUIInfoItemData, y: HistoryUIInfoItemData): number => {
				if (x.winNum < y.winNum) {
					return 1;
				} else if (x.winNum > y.winNum) {
					return -1;
				} else {
					return 0;
				}
			});
			betData[0].setTime(data.dataDetail[i].time);
			this.betData = this.betData.concat(betData);
		}
	}
}
class HistoryUIInfoItemData {
	public currentState: string;
	public time: string;
	public name: string;
	public winNum: string;
	public betNum: string;
	public constructor() {
		this.currentState = "up";
	}
	public setTime(time?: string): void {
		if (time) {
			let str: string[] = time.split("/");
			time = str[1] + "/" + str[2] + "\n" + str[3] + ":" + str[4];
		}
		this.time = time || "";
	}
	public setData(data: NetManager.HistoryBetData, time?: string): void {
		this.setTime(time);
		this.currentState = data.winNum==0 ? "up" : "down";
		this.betNum = Util.numToShow(data.betNum, 2);
		this.winNum = Util.numToShow(data.winNum, 2);

		switch (data.type) {
			case Game1Type.Symbol:
				this.name = "特码 " + Util.numToString(data.itemType);
				break;
			case Game1Type.NormalNumber:
				this.name = "平码-" + (Game1Config.GameInfo ? Game1Config.GameInfo.zodiacName[data.itemType] : " ");
				break;
			case Game1Type.Zodiac:
				if (data.itemType < 4) {
					this.name = Game1Config.GameInfo ? Game1Config.GameInfo.gameNameConfig[data.type].betTypeName[data.itemType] : " ";
				} else {
					let res: number[] = [];
					for (let item of data.data) {
						res.push(item);
					}
					res = res.sort((x: number, y: number): number => {
						if (x < y) {
							return -1;
						} else if (x > y) {
							return 1;
						} else {
							return 0;
						}
					});
					let resStr: string[] = [];
					for (let item of res) {
						resStr.push(Game1Config.GameInfo.zodiacName[item]);
					}
					this.name = resStr.join(",");
				}
				break;
			default:
				this.name = Game1Config.GameInfo ? Game1Config.GameInfo.gameNameConfig[data.type].betTypeName[data.itemType] : " ";
				break;
		}


	}
}  