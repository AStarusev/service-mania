var UIComponentClass = "eui.UIComponent";
class MiddleTableLayout extends eui.LayoutBase {
    private maxCount: number;
    private rowHeight: number;
    private columnWidth: number;
    private horizontalGap: number;
    private verticalGap: number;

    public constructor(maxCount: number, horizontalGap: number, verticalGap: number, rowHeight?: number, columnWidth?: number) {
        super();
        this.maxCount = maxCount;
        this.rowHeight = rowHeight;
        this.columnWidth = columnWidth;
        this.horizontalGap = horizontalGap;
        this.verticalGap = verticalGap;
    }
    /**
     * 计算target的尺寸
     * 因为环形布局，依赖容器尺寸来定义半径，所以需要容器显式的设置width和height,在这种情况下measure方法将失去作用
     * 所以在这个例子里面，不需要重写measure方法
     * 如果您的自定义布局需要根据内部子项计算尺寸，请重写这个方法
     **/
    public measure(): void {
        super.measure();
    }

    /**
     * 重写显示列表更新
     */
    public updateDisplayList(unscaledWidth: number, unscaledHeight: number): void {
        super.updateDisplayList(unscaledWidth, unscaledHeight);
        if (this.target == null)
            return;


        var centerX: number = unscaledWidth / 2;// 获得容器中心的X坐标
        var centerY: number = unscaledHeight / 2;// 获得容器中心的Y坐标
        var horizon: number = centerX / 2;// 获得水平可用长度的一半
        var vertical: number = centerY / 2;// 获得垂直可用长度的一半

        var count: number = this.target.numChildren;
        var maxX: number = 0;
        var maxY: number = 0;
        /// 第一轮循环收集可布局元素，或者说过滤不可布局元素
        var vcElemInLayout: Array<eui.UIComponent> = new Array<eui.UIComponent>();
        for (var i: number = 0; i < count; i++) {
            var layoutElement: eui.UIComponent = <eui.UIComponent>(this.target.getElementAt(i));
            if (!egret.is(layoutElement, UIComponentClass) || !layoutElement.includeInLayout) {
                /// 非布局元素需要排除在布局运算中

            } else {
                vcElemInLayout.push(layoutElement);
            }

        }
        count = vcElemInLayout.length;
        let pos: { x: number, y: number }[] = [];
        let len: number = count > this.maxCount ? Math.ceil(count / 2) : count;
        for (var i: number = 0; i < count; i++) {

            var elementWidth: number = this.columnWidth ? this.columnWidth : vcElemInLayout[i].width;
            var elementHeight: number = this.rowHeight ? this.rowHeight : vcElemInLayout[i].height;

            var childX: number = (i % len) * elementWidth + (i % len) * this.horizontalGap - elementWidth / 2;
            var childY: number = Math.floor(i / len) * elementHeight + Math.floor(i / len) * this.horizontalGap;

            vcElemInLayout[i].anchorOffsetX = vcElemInLayout[i].width / 2;
            vcElemInLayout[i].anchorOffsetY = vcElemInLayout[i].height / 2;
            pos.push({ x: childX, y: childY });

            maxX = Math.max(maxX, childX + elementWidth);
            maxY = Math.max(maxY, childY + elementHeight);
        }
        for (var i: number = 0; i < count; i++) {
            let diffX = i >= len ? (pos[count - 1].x - pos[len].x) / 2 : (pos[len - 1].x - pos[0].x) / 2;
            vcElemInLayout[i].setLayoutBoundsPosition(pos[i].x - diffX + centerX, pos[i].y);
            maxX = Math.max(maxX, pos[i].x - diffX + centerX);
        }
        this.target.setContentSize(maxX, maxY);
    }
}