class GameLobbyItemData {
	public unSelectedSource: string;
	public selectedSource: string;
	public type: Game1Type;
	private _betNum: number;
	public showBetNum: string;
	public constructor(type: Game1Type = null) {
		if (type != null) {
			this.setData(type);
		}
	}
	public setData(type: Game1Type) {
		this.type = type;
		this.unSelectedSource = Util.joinString("_", "lobby", Game1Type[type], "png");
		this.selectedSource = Util.joinString("_", "lobby", Game1Type[type], "select", "png");
		this.betNum = 0;
	}
	public get betNum(): number {
		return this._betNum;
	}
	public set betNum(num: number) {	
		this._betNum = num;
		this.showBetNum=Util.numToShow(this._betNum);
	}
}