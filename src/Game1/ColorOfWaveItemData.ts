class ColorOfWaveItemData{
	public unSelectedSource: string;
    public selectedSource: string;
	public symbolData:Array<SymbolItemData>;
	public type:ColorOfWaveType;
	public isShowBg:boolean;
	public constructor(type:ColorOfWaveType=null) {
		if(type!=null){
			this.setData(type);
		}
	}
	public setData(type:ColorOfWaveType){
		this.type=type;
		let symbols:SpecialCodeIFS[]=Game1.getColorOfWaveData(type);
		this.symbolData=[];
		for(let item of symbols){
			let symboldata:SymbolItemData=new SymbolItemData();
			symboldata.setData(item);
			this.symbolData.push(symboldata);
		}
		let title:string[]=["colorOfWave_red","colorOfWave_blue","colorOfWave_green"];
		this.unSelectedSource= Util.joinString("_", "rangeOfColor", ColorOfWaveType[type], "png");
    	this.selectedSource=Util.joinString("_", "rangeOfColor", ColorOfWaveType[type],"select", "png")
		this.isShowBg=false;
		if(type%2==0){
			this.isShowBg=true;
		}
	}

}
enum ColorOfWaveType{
	Red,Blue,Green
}	