module Game1Config {
	export let GameInfo:Game1ConfigData;
	export function constructor(){
		GameInfo=RES.getRes("game1Config_json");
	}
	export let width:number=360;
	export let height:number=592;
	
}
interface Game1NameData{
	gameType:number;
	name:string;
	prompt:string;
	betTypeName:string[];
}
interface Game1ConfigData{
	gameNameConfig:Game1NameData[];
	zodiacName:string[];
}