class SymbolItem extends eui.ItemRenderer implements eui.UIComponent {
	public constructor() {
		super();
		this.skinName = "SymbolItemSkin";
	}
	protected partAdded(partName: string, instance: any): void {
		super.partAdded(partName, instance);
	}

	private rect: eui.Rect;
	private touchGroup: eui.Group;

	protected childrenCreated(): void {
		super.childrenCreated();

		//this.touchGroup.addEventListener(egret.TouchEvent.TOUCH_TAP,this.touchThis,this);	

	}
	private touchThis(e: egret.TouchEvent): void {
		//this.selected=!this.selected;
	}
	protected dataChanged(): void {
	}

	public setData(data: SymbolItemData): void {
		this.data = data;
	}

}