class OddEven extends UIComponent  {
	private arrayCollection: eui.ArrayCollection;
	public constructor() {
		super();
		
	}

	protected partAdded(partName:string,instance:any):void
	{
		super.partAdded(partName,instance);
	}


	protected childrenCreated():void
	{
		super.childrenCreated();
		this.createUI();
	}
	public createUI():void	
	{
		let data:OddEvenItemData[]=[];
		for(let i =0;i<6;i++){
			let itemData:OddEvenItemData=new OddEvenItemData();
			itemData.setData(i as OddEvenType);
			data.push(itemData);
		}
		this.arrayCollection= new eui.ArrayCollection(data);
		this.uiList.itemRenderer=OddEvenItem;
		this.uiList.dataProvider=this.arrayCollection;
		this.arrayCollection.refresh();
	}
	
}