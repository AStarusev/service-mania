// TypeScript file
class SettlementItemData {
    public type: Game1Type;
    public itemType: number;
    public data: number[];
    public name: string;
    public betNum: number;
    public winNum: number;

    public constructor(type?: Game1Type) {
        this.type = type;
        this.betNum = 0;
        this.winNum = 0;
    }
    public setBetNum(num: number): void {
        this.betNum = num;
        if (this.type == Game1Type.Symbol) {
            this.winNum = this.betNum * (Game1.paytable[this.type][0]+1);
        } else {
            this.winNum = this.betNum * (Game1.paytable[this.type][this.itemType]+1);
        }
    }
}