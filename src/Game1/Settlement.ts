class Settlement extends eui.Panel implements eui.UIComponent {
	private dataGroup: eui.DataGroup;
	private arrayCollection: eui.ArrayCollection
	private allBetNum: eui.Label;
	private time: eui.Label;
	private phase: eui.Label;
	private submit: eui.Button;
	public constructor() {
		super();
		if (Game1.clientHeight / Game1.clientWidth > Game1Config.height / Game1Config.width) {
			let scale: number = Game1.clientWidth / Game1Config.width
			this.scaleX = scale;
			this.scaleY = scale;
			this.height = Game1.clientHeight / scale;
		} else {
			let scale: number = Game1.clientHeight / Game1Config.height
			this.scaleX = scale;
			this.scaleY = scale;
			this.width = Game1.clientWidth / scale;
		}
	}

	protected partAdded(partName: string, instance: any): void {
		super.partAdded(partName, instance);
	}


	protected childrenCreated(): void {
		super.childrenCreated();
		this.createUI();
		this.phase.text = "搅珠期数 " + Game1.phase;
		this.time.text = "截止售票时间 " + Game1.time;
		this.addEventListener(egret.Event.ADDED, this.resetUI, this);
		this.submit.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onTouchSubmit, this);
		this.addEventListener(UIComponentEvent.REMOVE_ITEM, this.onRemove, this);
	}
	private onTouchSubmit(e: egret.TouchEvent): void {
		if (this.submit.enabled) {
			
			if (!NetManager.placebet(Game1.getSettlementManager().getBetData())) {
				UIManager.showPromptPanel("no");
			}

			Game1.getSettlementManager().clear();
			UIManager.gameUINode.dispatchEvent(new UIComponentEvent(UIComponentEvent.UPDATEUI));
			this.parent.removeChild(this);
		}
	}
	public createUI(): void {
		let data: SettlementItemData[] = [];
		let allData: { [key: string]: SettlementItemData } = Game1.getSettlementManager().getData();
		for (let i in allData) {
			data.push(allData[i]);
		}
		this.arrayCollection = new eui.ArrayCollection(data);
		this.dataGroup.itemRenderer = SettlementItem;
		this.dataGroup.dataProvider = this.arrayCollection;
		this.arrayCollection.refresh();
	}
	public resetUI() {
		let data: SettlementItemData[] = [];
		let allData: { [key: string]: SettlementItemData } = Game1.getSettlementManager().getData()
		for (let i in allData) {
			data.push(allData[i]);
		}
		this.arrayCollection.replaceAll(data);
		this.updateAllBet();
		if (Game1.clientHeight / Game1.clientWidth > Game1Config.height / Game1Config.width) {
			let scale: number = Game1.clientWidth / Game1Config.width
			this.scaleX = scale;
			this.scaleY = scale;
			this.height = Game1.clientHeight / scale;
		} else {
			let scale: number = Game1.clientHeight / Game1Config.height
			this.scaleX = scale;
			this.scaleY = scale;
			this.width = Game1.clientWidth / scale;
		}
	}
	private onRemove(e: UIComponentEvent): void {
		Game1.getSettlementManager().removeData(e.target.data.name);
		this.arrayCollection.removeItemAt(e.target.itemIndex);
		this.updateAllBet();
		UIManager.gameUINode.dispatchEvent(new UIComponentEvent(UIComponentEvent.UPDATEUI));
	}
	private updateAllBet(): void {
		this.allBetNum.text = Util.numToShow(Game1.getSettlementManager().getAllBet());
		if (Game1.getSettlementManager().getAllBet() == 0 || Game1.getSettlementManager().getAllBet() > NetManager.getbalance()) {
			this.submit.enabled = false;
		} else {
			this.submit.enabled = true;
		}
	}

}
module Game1 {
	let _settlementManager;
	export function getSettlementManager(): SettlementManager {
		if (!_settlementManager) {
			_settlementManager = new SettlementManager();
		}
		return _settlementManager;
	}
	class SettlementManager {
		private pool: { [key: string]: SettlementItemData } = {};
		private allBetNum: number = 0;
		constructor() {
			
		}
		public addData(data: SettlementItemData): void {
			this.allBetNum += data.betNum;
			if (this.pool[data.name]) {
				this.pool[data.name].setBetNum(data.betNum + this.pool[data.name].betNum);
			} else {
				this.pool[data.name] = data;
			}

		}
		public getData(): { [key: string]: SettlementItemData } {
			return this.pool;
		}
		public removeData(name: string): void {
			this.allBetNum -= this.pool[name].betNum;
			delete this.pool[name];
		}
		public getAllBet(): number {
			return this.allBetNum;
		}
		public getBetNum(type: Game1Type): number {
			let res: number = 0;
			for (let i in this.pool) {

				if (this.pool[i].type == type) {
					res += this.pool[i].betNum;
				}
			}
			return res;
		}
		public getBetData(): NetManager.BetData[] {
			let data: NetManager.BetData[] = [];
			console.log(this.pool)
			for (let i in this.pool) {
				data.push({
					type: this.pool[i].type,
					itemType: this.pool[i].type == 0 ? this.pool[i].itemType + 1 : this.pool[i].itemType,
					data: this.pool[i].data||[],
					betNum: this.pool[i].betNum
				});
			}
			return data;
		}
		public clear() {
			this.allBetNum = 0;
			this.pool = {};
		}
	}

}