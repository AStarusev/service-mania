class HistoryUI extends eui.Component implements eui.UIComponent {
	private itemList: eui.DataGroup;
	private arrayCollection: eui.ArrayCollection;
	public constructor() {
		super();
		if (Game1.clientHeight / Game1.clientWidth > Game1Config.height / Game1Config.width) {
			let scale: number = Game1.clientWidth / Game1Config.width
			this.scaleX = scale;
			this.scaleY = scale;
			this.height = Game1.clientHeight / scale;
		} else {
			let scale: number = Game1.clientHeight / Game1Config.height
			this.scaleX = scale;
			this.scaleY = scale;
			this.width = Game1.clientWidth / scale;
		}
	}

	protected partAdded(partName: string, instance: any): void {
		super.partAdded(partName, instance);
	}


	protected childrenCreated(): void {
		super.childrenCreated();
		this.createUI();
		this.addEventListener(UIComponentEvent.REMOV_UICOMPONENT,()=>{
            this.parent.removeChild(this);
        },this);
	}
	public createUI(): void {
		let data: HistoryUIItemData[] = [];
		let history:NetManager.HistoryData[]=NetManager.getbethistory();
		if(history.length > 0){
			for (let i = 0; i < history.length; i++) {
				let itemData=new HistoryUIItemData();
				itemData.setData(history[i])
				data.push(itemData);
			}
			this.arrayCollection = new eui.ArrayCollection(data);
			this.itemList.itemRenderer = HistoryUIItem;
			this.itemList.dataProvider = this.arrayCollection;
			this.arrayCollection.refresh();
		}
	}
	public updateUI(){
		let data: HistoryUIItemData[] = [];
		let history:NetManager.HistoryData[]=NetManager.getbethistory();
		if(history.length > 0){
			for (let i = 0; i < history.length; i++) {
				let itemData=new HistoryUIItemData();
				itemData.setData(history[i])
				data.push(itemData);
			}
			this.arrayCollection.replaceAll(data);
			this.arrayCollection.refresh();
		}
		if (Game1.clientHeight / Game1.clientWidth > Game1Config.height / Game1Config.width) {
			let scale: number = Game1.clientWidth / Game1Config.width
			this.scaleX = scale;
			this.scaleY = scale;
			this.height = Game1.clientHeight / scale;
		} else {
			let scale: number = Game1.clientHeight / Game1Config.height
			this.scaleX = scale;
			this.scaleY = scale;
			this.width = Game1.clientWidth / scale;
		}
	}
	
}