declare module skins{
	class ButtonSkin extends eui.Skin{
	}
}
declare module skins{
	class CheckBoxSkin extends eui.Skin{
	}
}
declare module skins{
	class HScrollBarSkin extends eui.Skin{
	}
}
declare module skins{
	class HSliderSkin extends eui.Skin{
	}
}
declare module skins{
	class ItemRendererSkin extends eui.Skin{
	}
}
declare module skins{
	class PanelSkin extends eui.Skin{
	}
}
declare module skins{
	class ProgressBarSkin extends eui.Skin{
	}
}
declare module skins{
	class RadioButtonSkin extends eui.Skin{
	}
}
declare module skins{
	class ScrollerSkin extends eui.Skin{
	}
}
declare module skins{
	class TextInputSkin extends eui.Skin{
	}
}
declare module skins{
	class ToggleSwitchSkin extends eui.Skin{
	}
}
declare module skins{
	class VScrollBarSkin extends eui.Skin{
	}
}
declare module skins{
	class VSliderSkin extends eui.Skin{
	}
}
declare class BetInputSkin extends eui.Skin{
}
declare class BetUISkin extends eui.Skin{
}
declare class BigOrSmallSkin extends eui.Skin{
}
declare class BigOrSmallItemSkin extends eui.Skin{
}
declare class ColorOfWaveSkin extends eui.Skin{
}
declare class ColorOfWaveItemSkin extends eui.Skin{
}
declare class GameLobbySkin extends eui.Skin{
}
declare class GameLobbyItemSkin extends eui.Skin{
}
declare class GameUISkin extends eui.Skin{
}
declare class HistoryUISkin extends eui.Skin{
}
declare class HistoryUIInfoItemSkin extends eui.Skin{
}
declare class HistoryUIItemSkin extends eui.Skin{
}
declare class MenuRadioButtonSkin extends eui.Skin{
}
declare class NormalNumberSkin extends eui.Skin{
}
declare class OddEvenSkin extends eui.Skin{
}
declare class OddEvenItemSkin extends eui.Skin{
}
declare class PromptPanelSkin extends eui.Skin{
}
declare class ScrollSelectSkin extends eui.Skin{
}
declare class SettlementSkin extends eui.Skin{
}
declare class SettlementItemSkin extends eui.Skin{
}
declare class SymbolItemSkin extends eui.Skin{
}
declare class WaveSizeSkin extends eui.Skin{
}
declare class WaveSizeItemSkin extends eui.Skin{
}
declare class ZodiacSkin extends eui.Skin{
}
declare class ZodiacItemSkin extends eui.Skin{
}
