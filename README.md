# Lottery Application

## Development Environment
Follow the install [instructions](http://developer.egret.com/en/2d/projectConfig/installation).

Use the latest Egret Launcher (I used v1.0.51 in Jun 2018)
Install engine v5.0.15
Note that I had also installed engine 5.2.0 but the Egret Wing build gave me an error that I needed engine 5.0.15.
Install Egret Wing 4.1.5 or higher (this is the dev environment)

## Deployment
### Building in Debug:
Click in the upper right to toggle and show the Utility bar, choose the left option that looks like an open book (not the right option that looks like a wing).  Choose the build option (the button text is in Chinese).
Building within Egret Wing transpiles all of the typescript files and injects extra Egret specific code into the resulting javascript files.  The output files are created in the /bin-debug folder.
Hit the Run button (arrow inside a circle) on the Utility bar to run the app in your Chrome browser and hit F12 to view the debug console.  Note that calls to the cabbage proxy will fail and you'll see these errors in the console log.

### Building in Release:
To build in release to update into the cabbage apk, choose to Publish and enter a version number, let's assume is 1.0 for this example.  After the publish command is complete follow these steps:

1) In the bitboss-wallet project, delete everything within this directory: \www\assets\lottery
2) Go to the \lottery\bin-release\web\1.0\ folder and copy all files into the bitboss-wallet repo \www\assets\lottery folder
3) Revert any change that was made to the \www\assets\lottery\index.html file.  The version checked into the bitboss-wallet repo already has the proper data-scale-mode entry and should not be changed:
```
data-scale-mode="<fixedNarrow></fixedNarrow>"
```
4) Commit all of the lottery file changes into the bitboss-wallet repo.

## Development Notes
The main integration file is located in /src/Game1/NetManager.ts - this is where calls to the cabbage proxy are made and where data is returned back to the lottery UI tier.
