var returnRate = [
    [40],       //Special number
    [0.85,0.85,0.85,0.85,0.85,0.85], //Odd number,Even number,Big number, Small number, Combined odd, Combined even
    [0.85,0.85,0.85,0.85,10,2.4,1.6,1,0.85], //Beat zodiac signs, Livestock zodiac signs,Rat's side, Horse's side, One zodiac signs, Three zodiac signs, Four zodiac signs, Five zodiac signs, Six zodiac signs
    [2.5,2.5,2.5,2.5], //Odd big number, Odd small number, Even big number, Even small number
    [1.6,1.6,1.6],//Range of color: Red, Blue, Green
    [3.5,4,4,4,4,4],//Big or small number in one color: Red&small, Red&big, Blue&small, Blue&big, Green&small, Green&big
    [1,1,1,1,1,1,1,1,1,1,1,1] //Normal number:Rat, Ox, Tiger, Rabbit, Dragon, Snake, Horse, Goat, Monkey, Rooster, Gog, Pig
];

var commission = [  //default commission, if you don't want to use the  commission,you can zero setting the commission use the function setCommission()
    [0.1133],       //Special number
    [0.025,0.025,0.025,0.025,0.025,0.025], //Odd number,Even number,Big number, Small number, Combined odd, Combined even
    [0.025,0.025,0.025,0.025,0.033,0.1,0.0833,0.1167,0.025], //Beat zodiac signs, Livestock zodiac signs,Rat's side, Horse's side, One zodiac signs, Three zodiac signs, Four zodiac signs, Five zodiac signs, Six zodiac signs
    [0.075,0.075,0.075,0.075], //Odd big number, Odd small number, Even big number, Even small number
    [0.048,0.101,0.101],//Range of color: Red, Blue, Green
    [0.0125,0.2208,0.2208,0.0125,0.2208,0.1167],//Big or small number in one color: Red&small, Red&big, Blue&small, Blue&big, Green&small, Green&big
    [0.0066,0.0066,0.0066,0.0066,0.0066,0.0066,0.0066,0.0066,0.0066,0.0066,0.0066,0.0066] //Normal number:Rat, Ox, Tiger, Rabbit, Dragon, Snake, Horse, Goat, Monkey, Rooster, Gog, Pig
];

var zodiacTableData = [  //They can be rewrited recording to the current year
    [01,13,25,37,49],
    [12,24,36,48],
    [11,23,35,47],
    [10,22,34,46],
    [9,21,33,45],
    [8,20,32,44],
    [7,19,31,43],
    [6,18,30,42],
    [5,17,29,41],
    [4,16,28,40],
    [3,15,27,39],
    [2,14,26,38]];

var rangeOfColor = [//Range of color
    [1,2,7,8,12,13,18,19,23,24,29,30,34,35,40,45],//Red
    [3,4,9,10,14,15,20,25,26,31,36,37,41,42,47,48],//Blue
    [5,6,11,16,17,21,22,27,28,32,33,38,39,43,44,49]//Green
];

function getReturnRate() {

    var returnRateOfCurrentYearZodiac = 0.7;
    //Recalculate the zodiac signs based on the current year
    var myDate = new Date();
    var fullYear = myDate.getFullYear();
    var thisYrarZodiac = ((fullYear+8)%12);//The current year's zodiac sign 0-11

    returnRate[6][thisYrarZodiac] = returnRateOfCurrentYearZodiac;

    return returnRate;
}

function setReturnRate(array) {
    /*
    array format

     var returnRate = [
     [40],       //Special number
     [0.85,0.85,0.85,0.85,0.85,0.85], //Odd number,Even number,Big number, Small number, Combined odd, Combined even
     [0.85,0.85,0.85,0.85,10,2.4,1.6,1,0.85], //Beat zodiac signs, Livestock zodiac signs,Rat's side, Horse's side, One zodiac signs, Three zodiac signs, Four zodiac signs, Five zodiac signs, Six zodiac signs
     [2.5,2.5,2.5,2.5], //Odd big number, Odd small number, Even big number, Even small number
     [1.6,1.6,1.6],//Range of color: Red, Blue, Green
     [3.5,4,4,4,4,4],//Big or small number in one color: Red&small, Red&big, Blue&small, Blue&big, Green&small, Green&big
     [1,1,1,1,1,1,1,1,1,1,1,1] //Normal number:Rat, Ox, Tiger, Rabbit, Dragon, Snake, Horse, Goat, Monkey, Rooster, Dog, Pig
     ]
     */
    returnRate = array.slice();
}

function getCommission() {

    var commissionOfCurrentYearZodiac = 0.0084;
    //Recalculate the zodiac signs based on the current year
    var myDate = new Date();
    var fullYear = myDate.getFullYear();
    var thisYrarZodiac = ((fullYear+8)%12);//The current year's zodiac sign 0-11

    commission[6][thisYrarZodiac] = commissionOfCurrentYearZodiac;

    return commission;
}

function setCommission(array) {
    /*
    array format
     var commission = [  //default commission
     [0.1133],       //Special number
     [0.025,0.025,0.025,0.025,0.025,0.025], //Odd number,Even number,Big number, Small number, Combined odd, Combined even
     [0.025,0.025,0.025,0.025,0.033,0.1,0.0833,0.1167,0.025], //Beat zodiac signs, Livestock zodiac signs,Rat's side, Horse's side, One zodiac signs, Three zodiac signs, Four zodiac signs, Five zodiac signs, Six zodiac signs
     [0.075,0.075,0.075,0.075], //Odd big number, Odd small number, Even big number, Even small number
     [0.048,0.101,0.101],//Range of color: Red, Blue, Green
     [0.0125,0.2208,0.2208,0.0125,0.2208,0.1167],//Big or small number in one color: Red&small, Red&big, Blue&small, Blue&big, Green&small, Green&big
     [0.0066,0.0066,0.0066,0.0066,0.0066,0.0066,0.0066,0.0066,0.0066,0.0066,0.0066,0.0066] //Normal number:Rat, Ox, Tiger, Rabbit, Dragon, Snake, Horse, Goat, Monkey, Rooster, Gog, Pig
     ]
     */
    commission = array.slice();
}

function getZodiacTableData(){
    var zodiacTable=[];//zodiac signs

    //Recalculate the zodiac signs based on the current year
    var myDate = new Date();
    var fullYear = myDate.getFullYear();
    var thisYrarZodiac = ((fullYear+8)%12);//The current year's zodiac sign 0-11
    for(var i=0;i<12;++i)
    {
        zodiacTable[(thisYrarZodiac+i)%12] = zodiacTableData[i];
    }

    return zodiacTable;
}

function getLotteryResult(gameID) { //You can add your function which can get the LotteryResult from http://bet.hkjc.com/marksix/index.aspx?lang=ch
    var marksix = "marksix";
    //You can add new games like this:
    // var newGameName = "newGameName";

    var lotteryResult = [11,22,43,4,15,6,7]; //The value is a data of list, common number(6)+special number(1) like lotteryResult = [11,22,43,4,15,6,7]
    if(gameID === marksix )
    {
        var http = require('http');
        var request_ = require('request');
        var urlencode2=require("urlencode2");
        var url=require('url')
        http.createServer(function (request, response) {
            var arg1 = url.parse(request.url, true).query;
            var sn=arg1.sn;
            var en=arg1.en;
            var req_url="http://bet.hkjc.com/marksix/index.aspx?lang=ch?qt=nav&c=131&sn=2%24%24%24%24%24%24%20"+
                urlencode2(sn,'gbk')+"%24%240%24%24%24%24&en=2%24%24%24%24%24%24"+
                urlencode2(en,'gbk')+"%24%240%24%24%24%24&sy=0&ie=utf-8&oue=1&fromproduct=jsapi&res=api&callback=BMap._rd._cbk54249";
            request_.get({
                    url:req_url,
                    json:true
                },
                function(error, response_, body) {
                    if (!error && response_.statusCode == 200) {
                        var res=-1;
                        if(body){
                            res=body.split(',"toll":')[0];//time  s
                            res=res.split('"time":')[2];
                            console.log(res)
                            if(!res){
                                res=-1;
                            }
                            else{
                                res=res/60;
                            }
                        }
                        response.writeHead(200, {
                            "Content-Type": "text/html; charset=UTF-8",
                            'Access-Control-Allow-Origin':request.headers.origin
                        });
                        response.end(res+'\n');
                    }
                    else{
                        // console.log(error)
                    }
                }
            )
        }).listen(8888);
        
        return lotteryResult;

    }
}

function getLotteryEndTime(gameID) {
    //You can add your function which can get the LotteryEndTime from http://bet.hkjc.com/marksix/index.aspx?lang=ch
    var lotteryEndTime =  ['2018/04/26/09/15','18/045'];
    var marksix = "marksix";
    if(gameID === marksix ) {
        var http = require('http');
        var request_ = require('request');
        var urlencode2=require("urlencode2");
        var url=require('url')
        http.createServer(function (request, response) {
            var arg1 = url.parse(request.url, true).query;
            var sn=arg1.sn;
            var en=arg1.en;
            var req_url="http://bet.hkjc.com/marksix/index.aspx?lang=ch?qt=nav&c=131&sn=2%24%24%24%24%24%24%20"+
                urlencode2(sn,'gbk')+"%24%240%24%24%24%24&en=2%24%24%24%24%24%24"+
                urlencode2(en,'gbk')+"%24%240%24%24%24%24&sy=0&ie=utf-8&oue=1&fromproduct=jsapi&res=api&callback=BMap._rd._cbk54249";
            request_.get({
                    url:req_url,
                    json:true
                },
                function(error, response_, body) {
                    if (!error && response_.statusCode == 200) {
                        var res=-1;
                        if(body){
                            res=body.split(',"toll":')[0];//time  s
                            res=res.split('"time":')[2];
                            console.log(res)
                            if(!res){
                                res=-1;
                            }
                            else{
                                res=res/60;
                            }
                        }
                        response.writeHead(200, {
                            "Content-Type": "text/html; charset=UTF-8",
                            'Access-Control-Allow-Origin':request.headers.origin
                        });
                        response.end(res+'\n');
                    }
                    else{
                        // console.log(error)
                    }
                }
            )
        }).listen(8888);

        return lotteryEndTime;
    }
}

function getUserWinResult(betData) {

    /*
     betData format
     betData:[
        {
            type: GameType;
            itemType: number;
            data: number[];
             betNum: number;
        }
     ]

     ***Lottery History data***
     data {
        phase:"18/025",
        time:"2018/04/25/09/15",
        lotteryNum:[11,22,43,4,15,6,7],
        dataDetail:[
            {
                time:"2018/04/25/09/15",
                betData:[
                    {
                        type: GameType;
                        itemType: number;
                        data: number[];
                        betNum: number;
                        winNum:number;
                    }
                ]
            }
        ]
     }
     */

    var GameType = {
        "Symbol": 0, "OddEven": 1, "Zodiac": 2, "BigOrSmall": 3, "RangeOfColor": 4, "ColorSize": 5, "NormalNumber": 6
    };

    var BigOrSmallType = {
        "OddBig": 0, "OddSmall": 1, "EvenBig": 2, "EvenSmall": 3
    };

    var RangeOfColorType = {
        "Red": 0, "Blue": 1, "Green": 2
    };

    var OddEvenType = {
        "Odd": 0, "Even": 1, "Big": 2, "Small": 3, "CombinedOdd": 4, "CombinedEven": 5
    };

    var ColorSizeType = {
        "RedSmall": 0, "RedBig": 1, "BlueSmall": 2, "BlueBig": 3, "GreenSmall": 4, "GreenBig": 5
    };

    var ZodiacCategoryType = {
        "BeastZodiac": 0,
        "LivestockZodiac": 1,
        "RatSide": 2,
        "HorseSide": 3,
        "OneZodiacSign": 4,
        "ThreeZodiacSigns": 5,
        "FourZodiacSigns": 6,
        "FiveZodiacSigns": 7,
        "SixZodiacSigns": 8
    };

    var ZodiacType = {
        "Rat": 0,
        "Ox": 1,
        "Tiger": 2,
        "Rabbit": 3,
        "Dragon": 4,
        "Snake": 5,
        "Horse": 6,
        "Goat": 7,
        "Monkey": 8,
        "Rooster": 9,
        "Dog": 10,
        "Pig": 11
    };

    var currentLotteryResult = getLotteryResult("marksix");
    var currentReturnRate = getReturnRate();
    var currentCommission = getCommission();
    var currentZodiacTable = getZodiacTableData();

    var allWin = 0;
    var specialNumberCombined = currentLotteryResult[6] % 10 + currentLotteryResult[6] / 10;
    var specialNumberIsOdd = currentLotteryResult[6] % 2 == 1 ? true : false;
    var specialNumberIsEven = currentLotteryResult[6] % 2 == 0 ? true : false;
    var specialNumberIsBig = currentLotteryResult[6] > 24 ? true : false;
    var specialNumberIsSmall = currentLotteryResult[6] < 25 ? true : false;
    var specialNumberCombinedIsOdd = specialNumberCombined % 2 == 1 ? true : false;
    var specialNumberCombinedIsEven = specialNumberCombined % 2 == 0 ? true : false;

    var isRangeOfColor = [false,false,false];

    for(var i = 0; i < isRangeOfColor.length; i++) {
        for(var k = 0 ; k < rangeOfColor[i].length; k++) {
            if (currentLotteryResult[6] == rangeOfColor[i][k]) {
                isRangeOfColor[i] = true;
            }
        }

    }

    for (var i = 0; i < betData.length; i++) {
        switch (betData[i].type) {
            case GameType.Symbol:  //Bet special number
            {
                if (currentLotteryResult[6] == betData[i].itemType) {
                    allWin += betData[i].betNum * (1 + currentReturnRate[0][0]) + betData[i].betNum * currentCommission[0][0];
                }
                break;
            }
            case GameType.OddEven:
            {
                if (currentLotteryResult[6] != 49) {
                    if (specialNumberIsOdd && (betData[i].itemType == OddEvenType.Odd)) {
                        allWin += betData[i].betNum * (1 + currentReturnRate[1][0]) + betData[i].betNum * currentCommission[1][0];
                    }
                    else if (specialNumberIsEven && (betData[i].itemType == OddEvenType.Even)) {
                        allWin += betData[i].betNum * (1 + currentReturnRate[1][1]) + betData[i].betNum * currentCommission[1][1];
                    }
                    else if (specialNumberIsBig && (betData[i].itemType == OddEvenType.Big)) {
                        allWin += betData[i].betNum * (1 + currentReturnRate[1][2]) + betData[i].betNum * currentCommission[1][2];
                    }
                    else if (specialNumberIsSmall && (betData[i].itemType == OddEvenType.Small)) {
                        allWin += betData[i].betNum * (1 + currentReturnRate[1][3]) + betData[i].betNum * currentCommission[1][3];
                    }
                    else if (specialNumberCombinedIsOdd && (betData[i].itemType == OddEvenType.CombinedOdd)) {
                        allWin += betData[i].betNum * (1 + currentReturnRate[1][4]) + betData[i].betNum * currentCommission[1][4];
                    }
                    else if (specialNumberCombinedIsEven && (betData[i].itemType == OddEvenType.CombinedEven)) {
                        allWin += betData[i].betNum * (1 + currentReturnRate[1][5]) + betData[i].betNum * currentCommission[1][5];
                    }
                }
                break;
            }
            case GameType.Zodiac:
            {
                if (currentLotteryResult[6] != 49) {
                    var specialNumberZodiac = -1;
                    var specialNumberIsBeast = false;
                    var specialNumberIsLivestock = false;
                    var specialNumberIsRatSide = false;
                    var specialNumberIsHorseSide = false;
                    for (var i = 0; i < 12; ++i) {
                        for (var j = 0; j < 5; ++j) {
                            if (currentLotteryResult[6] === currentZodiacTable[i][j]) {
                                specialNumberZodiac = i;
                                break;
                            }
                        }
                    }

                    if ((specialNumberZodiac == 0) || (specialNumberZodiac == 2) || (specialNumberZodiac == 3) ||
                        (specialNumberZodiac == 4) || (specialNumberZodiac == 5) || (specialNumberZodiac == 8)) {
                        specialNumberIsBeast = true;
                    }
                    else {
                        specialNumberIsLivestock = true;
                    }

                    if ((specialNumberZodiac == 0) || (specialNumberZodiac == 1) || (specialNumberZodiac == 2) ||
                        (specialNumberZodiac == 3) || (specialNumberZodiac == 4) || (specialNumberZodiac == 5)) {
                        specialNumberIsRatSide = true;
                    }
                    else {
                        specialNumberIsHorseSide = true;
                    }

                    if (specialNumberIsBeast && (betData[i].itemType == ZodiacCategoryType.BeastZodiac)) {
                        allWin += betData[i].betNum * (1 + currentReturnRate[2][0]) + betData[i].betNum * currentCommission[2][0];
                    }
                    else if (specialNumberIsLivestock && (betData[i].itemType == ZodiacCategoryType.LivestockZodiac)) {
                        allWin += betData[i].betNum * (1 + currentReturnRate[2][1]) + betData[i].betNum * currentCommission[2][1];
                    }
                    else if (specialNumberIsRatSide && (betData[i].itemType == ZodiacCategoryType.RatSide)) {
                        allWin += betData[i].betNum * (1 + currentReturnRate[2][2]) + betData[i].betNum * currentCommission[2][2];
                    }
                    else if (specialNumberIsHorseSide && (betData[i].itemType == ZodiacCategoryType.HorseSide)) {
                        allWin += betData[i].betNum * (1 + currentReturnRate[2][3]) + betData[i].betNum * currentCommission[2][3];
                    }
                    else if ((betData[i].itemType == ZodiacCategoryType.OneZodiacSign) && (betData[i].data[0] == specialNumberZodiac)) {
                        allWin += betData[i].betNum * (1 + currentReturnRate[2][4]) + betData[i].betNum * currentCommission[2][4];
                    }
                    else {
                        for(var j = ZodiacCategoryType.OneZodiacSign; j < ZodiacCategoryType.length; j++) {
                            var k = 0;
                            for (; k < betData[i].data.length; k++) {
                                if ((specialNumberZodiac == betData[i].data[k])&&(betData[i].itemType == j)) {
                                    allWin += betData[i].betNum * (1 + currentReturnRate[2][j]) + betData[i].betNum * currentCommission[2][j];
                                    break;
                                }
                            }
                            if(k < betData[i].data.length)
                                break;
                        }
                    }


                }
                break;
            }
            case GameType.BigOrSmall:
            {
                if (currentLotteryResult[6] != 49) {
                    if (specialNumberIsOdd && specialNumberIsBig && (betData[i].itemType == BigOrSmallType.OddBig)) {
                        allWin += betData[i].betNum * (1 + currentReturnRate[3][0]) + betData[i].betNum * currentCommission[3][0];
                    }
                    else if (specialNumberIsOdd && specialNumberIsSmall && (betData[i].itemType == BigOrSmallType.OddSmall)) {
                        allWin += betData[i].betNum * (1 + currentReturnRate[3][1]) + betData[i].betNum * currentCommission[3][1];
                    }
                    else if (specialNumberIsEven && specialNumberIsBig && (betData[i].itemType == BigOrSmallType.EvenBig)) {
                        allWin += betData[i].betNum * (1 + currentReturnRate[3][2]) + betData[i].betNum * currentCommission[3][2];
                    }
                    else if (specialNumberIsEven && specialNumberIsSmall && (betData[i].itemType == BigOrSmallType.EvenSmall)) {
                        allWin += betData[i].betNum * (1 + currentReturnRate[3][3]) + betData[i].betNum * currentCommission[3][3];
                    }

                }
                break;
            }
            case GameType.RangeOfColor:
            {
                for(var i = 0; i < rangeOfColor.length; i++) {
                    var k = 0;
                    for (; k < rangeOfColor[i].length; k++) {
                        if ((currentLotteryResult[6] == rangeOfColor[i][k])&&(betData[i].itemType == RangeOfColorType.Red)) {
                            allWin += betData[i].betNum * (1 + currentReturnRate[4][i]) + betData[i].betNum * currentCommission[4][i];
                            break;
                        }
                    }
                    if(k < rangeOfColor[i].length)
                        break;
                }
                break;
            }
            case GameType.ColorSize:
            {
                if (currentLotteryResult[6] != 49) {

                    if(isRangeOfColor[0] && specialNumberIsSmall && (betData[i].itemType == ColorSizeType.RedSmall)) {
                        allWin += betData[i].betNum * (1 + currentReturnRate[5][0]) + betData[i].betNum * currentCommission[5][0];
                    }
                    else if(isRangeOfColor[0] && specialNumberIsBig && (betData[i].itemType == ColorSizeType.RedBig)) {
                        allWin += betData[i].betNum * (1 + currentReturnRate[5][1]) + betData[i].betNum * currentCommission[5][1];
                    }
                    else if(isRangeOfColor[1] && specialNumberIsSmall && (betData[i].itemType == ColorSizeType.BlueSmall)) {
                        allWin += betData[i].betNum * (1 + currentReturnRate[5][2]) + betData[i].betNum * currentCommission[5][2];
                    }
                    else if(isRangeOfColor[1] && specialNumberIsBig && (betData[i].itemType == ColorSizeType.BlueBig)) {
                        allWin += betData[i].betNum * (1 + currentReturnRate[5][3]) + betData[i].betNum * currentCommission[5][3];
                    }
                    else if(isRangeOfColor[2] && specialNumberIsSmall && (betData[i].itemType == ColorSizeType.GreenSmall)) {
                        allWin += betData[i].betNum * (1 + currentReturnRate[5][4]) + betData[i].betNum * currentCommission[5][4];
                    }
                    else if(isRangeOfColor[2] && specialNumberIsBig && (betData[i].itemType == ColorSizeType.GreenBig)) {
                        allWin += betData[i].betNum * (1 + currentReturnRate[5][5]) + betData[i].betNum * currentCommission[5][5];
                    }
                }

                break;
            }
            case GameType.NormalNumber:
            {
                var isZodiacWinning = [false,false,false,false,false,false,false,false,false,false,false,false];
                for(j = 0; j < currentLotteryResult.length; j++) {
                    for (var k = 0; k < currentZodiacTable.length; k++) {
                        for (var l = 0; l < currentZodiacTable[k].length; l++) {
                            if (currentLotteryResult[j] == currentZodiacTable[k][l]) {
                                isZodiacWinning[k] = true;
                            }
                        }
                    }
                }
                if(isZodiacWinning[betData[i].itemType] == true) {
                    allWin += betData[i].betNum * (1 + currentReturnRate[6][betData[i].itemType]) + betData[i].betNum * currentCommission[6][betData[i].itemType];
                }

                break;
            }
        }
    }
}
    































